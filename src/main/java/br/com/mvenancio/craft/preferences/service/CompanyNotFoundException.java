package br.com.mvenancio.craft.preferences.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class CompanyNotFoundException extends CraftException {

	private static final long serialVersionUID = -1908685198099846108L;

	public CompanyNotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}

}
