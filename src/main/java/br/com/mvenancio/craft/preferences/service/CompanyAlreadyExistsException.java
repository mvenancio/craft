package br.com.mvenancio.craft.preferences.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class CompanyAlreadyExistsException extends CraftException {

	private static final long serialVersionUID = 8743230088384814958L;

	public CompanyAlreadyExistsException(String message) {
		super(message, HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
