package br.com.mvenancio.craft.preferences.repository;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.preferences.model.Company;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class CompanyRepository extends JpaRepository<Company, Long> {

	@Override
	protected Class<Company> getEntityClass() {
		return Company.class;
	}

	public Company findByUser(User user) {
		return this.queryOne(new JpaNamedQuery<>(getEntityClass(), Company.FIND_BY_USER, user));
	}

}
