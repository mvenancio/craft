package br.com.mvenancio.craft.preferences.facade.json;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.preferences.model.Company;

@Component
public class CompanyAdapter {

	public CompanyJson toJson(Company entity) {
		CompanyJson json = new CompanyJson();
		json.setId(entity.getId());
		json.setAddress(entity.getAddress());
		json.setFacebook(entity.getFacebook());
		json.setGooglePlus(entity.getGooglePlus());
		json.setName(entity.getName());
		json.setPhone(entity.getPhone());
		json.setTwitter(entity.getTwitter());
		return json;
	}

}
