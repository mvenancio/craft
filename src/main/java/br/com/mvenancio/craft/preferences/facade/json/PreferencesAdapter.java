package br.com.mvenancio.craft.preferences.facade.json;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.preferences.model.Preferences;

@Component
public class PreferencesAdapter {

	public PreferencesJson toJson(Preferences entity) {
		PreferencesJson json = new PreferencesJson();
		json.setId(entity.getId());
		json.setHours(entity.getHours());
		json.setMargin(entity.getMargin());
		json.setProfit(entity.getProfit());
		json.setImage(entity.getImage());
		json.setValueByHour(entity.getValueByHour());
		return json;
	}

}
