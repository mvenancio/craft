package br.com.mvenancio.craft.preferences.facade.json;

import java.math.BigDecimal;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.image.model.Image;

public class PreferencesJson extends AbstractJson {

	private Long id;

	private BigDecimal profit;

	private Long hours;

	private BigDecimal valueByHour;

	private BigDecimal margin;

	private Image image;

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public Long getHours() {
		return hours;
	}

	public void setHours(Long hours) {
		this.hours = hours;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getValueByHour() {
		return valueByHour;
	}

	public void setValueByHour(BigDecimal valueByHour) {
		this.valueByHour = valueByHour;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

}
