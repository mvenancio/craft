package br.com.mvenancio.craft.preferences.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.preferences.facade.json.CompanyJson;
import br.com.mvenancio.craft.preferences.model.Company;
import br.com.mvenancio.craft.preferences.repository.CompanyRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class CompanyService {

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyRepository repository;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Company findByUser(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		return repository.findByUser(user);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Company add(CompanyJson json, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);

		Company company = repository.findByUser(user);
		if (company != null) {
			throw new CompanyAlreadyExistsException("Company already exists for given user ");
		}
		company = new Company();
		company.setAddress(json.getAddress());
		company.setFacebook(json.getFacebook());
		company.setGooglePlus(json.getGooglePlus());
		company.setName(json.getName());
		company.setPhone(json.getPhone());
		company.setTwitter(json.getTwitter());
		company.setUser(user);
		return repository.add(company);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Company get(Long companyId, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Company company = repository.get(companyId);
		if (company == null || !company.getUser().equals(user)) {
			throw new CompanyNotFoundException("Company not found: id " + companyId);
		}
		return company;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Company update(Long id, CompanyJson json, String encodedUserId) {
		Company company = get(id, encodedUserId);
		company.setAddress(json.getAddress());
		company.setFacebook(json.getFacebook());
		company.setGooglePlus(json.getGooglePlus());
		company.setName(json.getName());
		company.setPhone(json.getPhone());
		company.setTwitter(json.getTwitter());
		return repository.update(company);
	}

}
