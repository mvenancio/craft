package br.com.mvenancio.craft.preferences.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.image.model.Image;
import br.com.mvenancio.craft.security.model.User;

@Entity
@Table(name = "preferences")
@NamedQuery(name = Preferences.FIND_BY_USER, query = "SELECT p FROM Preferences p WHERE p.user = ?1 ORDER BY p.id")
public class Preferences {

	private static final String ID_SEQ = "preferences_id_seq";
	public static final String FIND_BY_USER = "Preferences-FindByUser";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	private BigDecimal profit;

	private Long hours;

	private BigDecimal valueByHour;

	private BigDecimal margin;
	
	@Column(name="estimates_expiration_days")
	private Long estimatesExpirationDays;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;
	
	@OneToOne
	@JoinColumn(name = "image_id")
	private Image image;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getProfit() {
		return profit;
	}

	public void setProfit(BigDecimal profit) {
		this.profit = profit;
	}

	public Long getHours() {
		return hours;
	}

	public void setHours(Long hours) {
		this.hours = hours;
	}

	public BigDecimal getValueByHour() {
		return valueByHour;
	}

	public void setValueByHour(BigDecimal valueByHour) {
		this.valueByHour = valueByHour;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getEstimatesExpirationDays() {
		return estimatesExpirationDays;
	}

	public void setEstimatesExpirationDays(Long estimatesExpirationDays) {
		this.estimatesExpirationDays = estimatesExpirationDays;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

}
