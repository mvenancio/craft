package br.com.mvenancio.craft.preferences.repository;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.preferences.model.Preferences;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class PreferencesRepository extends JpaRepository<Preferences, Long> {

	@Override
	protected Class<Preferences> getEntityClass() {
		return Preferences.class;
	}

	public Preferences findByUser(User user) {
		return this.queryOne(new JpaNamedQuery<>(getEntityClass(), Preferences.FIND_BY_USER, user));
	}

}
