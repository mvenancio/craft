package br.com.mvenancio.craft.preferences.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Entity
@Table(name = "company")
@NamedQuery(name = Company.FIND_BY_USER, query = "SELECT c FROM Company c WHERE c.user = ?1 ORDER BY c.id")
public class Company {

	private static final String ID_SEQ = "company_id_seq";
	public static final String FIND_BY_USER = "Company-FindByUser";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	private String name;

	private String address;

	private String phone;

	private String facebook;

	private String googlePlus;

	private String twitter;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getGooglePlus() {
		return googlePlus;
	}

	public void setGooglePlus(String googlePlus) {
		this.googlePlus = googlePlus;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
