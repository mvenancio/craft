package br.com.mvenancio.craft.preferences.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.image.repository.ImageRepository;
import br.com.mvenancio.craft.preferences.facade.json.PreferencesJson;
import br.com.mvenancio.craft.preferences.model.Preferences;
import br.com.mvenancio.craft.preferences.repository.PreferencesRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class PreferencesService {

	@Autowired
	private UserService userService;
	
	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private PreferencesRepository repository;

	@Transactional(propagation = Propagation.REQUIRED)
	public Preferences add(PreferencesJson json, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);

		Preferences preferences = repository.findByUser(user);
		if (preferences != null) {
			return update(preferences.getId(), json, encodedUserId);
		}
		
		preferences = new Preferences();
		fillPreferences(json, preferences);
		preferences.setUser(user);
		return repository.add(preferences);
	}

	private void fillPreferences(PreferencesJson json, Preferences preferences) {
		preferences.setHours(json.getHours());
		preferences.setMargin(json.getMargin());
		preferences.setProfit(json.getProfit());
		preferences.setValueByHour(json.getValueByHour());
		if(json.getImage() != null) {
			preferences.setImage(imageRepository.findOne(json.getImage().getId()));
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Preferences get(Long preferenceId, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Preferences preferences = repository.get(preferenceId);
		if (preferences == null || !preferences.getUser().equals(user)) {
			throw new PreferencesNotFoundException("Preference not found: " + preferenceId);
		}
		return preferences;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Preferences update(Long id, PreferencesJson json, String encodedUserId) {
		Preferences preferences = get(id, encodedUserId);
		fillPreferences(json, preferences);
		return repository.update(preferences);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Preferences findByUser(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Preferences preference = repository.findByUser(user);
		return preference != null ? preference : add(new PreferencesJson(), encodedUserId);
	}
}
