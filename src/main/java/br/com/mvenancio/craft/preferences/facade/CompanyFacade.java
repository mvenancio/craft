package br.com.mvenancio.craft.preferences.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.preferences.facade.json.CompanyAdapter;
import br.com.mvenancio.craft.preferences.facade.json.CompanyJson;
import br.com.mvenancio.craft.preferences.service.CompanyService;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/company")
public class CompanyFacade {

	@Autowired
	private CompanyService service;

	@Autowired
	private CompanyAdapter adapter;

	@Autowired
	private ResponseUtil<CompanyJson> responseUtil;

	@RequestMapping(value = "/user", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getByUser(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.findByUser(encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(@PathVariable("id") Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.get(id, encodedUserId)));
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@RequestBody CompanyJson json,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.add(json, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> put(@PathVariable("id") Long id, @RequestBody CompanyJson json,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.update(id, json, encodedUserId)));
	}

}
