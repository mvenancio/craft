package br.com.mvenancio.craft.preferences.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class PreferencesAlreadyExistsException extends CraftException {

	private static final long serialVersionUID = -9058566564260189814L;

	public PreferencesAlreadyExistsException(String message) {
		super(message, HttpStatus.UNPROCESSABLE_ENTITY);
	}

}
