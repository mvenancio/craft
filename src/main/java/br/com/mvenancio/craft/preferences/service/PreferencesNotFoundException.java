package br.com.mvenancio.craft.preferences.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class PreferencesNotFoundException extends CraftException {

	private static final long serialVersionUID = -9058566564260189814L;

	public PreferencesNotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}

}
