package br.com.mvenancio.craft.commons.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.google.common.base.Preconditions;

@Repository
public abstract class JpaRepository<E, K extends Serializable> {

    /**
     * The persistence context for this object.
     */
    @PersistenceContext
    protected EntityManager em;

    /**
     * Provides entity class.
     */
    protected abstract Class<E> getEntityClass();

    /*
     * (non-Javadoc)
     *
     * @see IRepository#get(java.lang.Object)
     */
    public E get(K id) {
        return em.find(getEntityClass(), id);
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#get(java.lang.Object, boolean)
     */
    public E get(K id, boolean lock) {
        if (!lock) {
            return get(id);
        }
        em.flush();
        final E entity = em.find(getEntityClass(), id, LockModeType.PESSIMISTIC_READ);
        if (entity != null) {
            em.refresh(entity);
        }
        return entity;
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#query(IQuery)
     */
    public List<E> query(JpaQuery<E> spec) {
        final JpaQueryStrategy strategy = spec.getStrategy();
        return strategy.query(em, spec);
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#queryOne(IQuery)
     */
    public E queryOne(JpaQuery<E> spec) {
        final List<E> result = query(spec);
        Preconditions.checkState(!(result.size() > 1), "Query one should result in only one entity.");
        return result.isEmpty() ? null : result.get(0);
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#queryOne(IQuery, boolean)
     */
    public E queryOne(JpaQuery<E> spec, boolean lock) {
        if (!lock) {
            return queryOne(spec);
        }
        final E entity = queryOne(new JpaQueryLockDecorator<>(spec));
        if (entity != null) {
            em.refresh(entity);
        }
        return entity;
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#get(java.lang.Object, boolean)
     */
    public List<E> queryAll() {
        final String select = "SELECT e FROM " + getEntityClass().getSimpleName() + " e";
        return em.createQuery(select, getEntityClass()).getResultList();
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#add(E)
     */
    public E add(E entity) {
        em.persist(entity);
        return entity;
    }

    /**
     * Adding all Entities in the repository.
     *
     * @param entities
     *            a list of Entities
     */
    public void addAll(Collection<E> entities) {
        for (final E entity : entities) {
            add(entity);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#update(AbstractEntity)
     */
    public E update(E entity) {
        return em.merge(entity);
    }

    /*
     * (non-Javadoc)
     *
     * @see IRepository#remove(AbstractEntity)
     */
    public void remove(E entity) {
        em.remove(entity);
    }

    /**
     * Delete all Entities in the repository.
     */
    public void removeAll() {
        final String delete = "DELETE FROM " + getEntityClass().getSimpleName();
        em.createQuery(delete).executeUpdate();
    }

}
