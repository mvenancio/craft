package br.com.mvenancio.craft.commons.repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public abstract class JpaQueryDecorator<E> extends JpaQuery<E> {

    private final JpaQuery<E> delegate;

    /**
     * Constructor.
     * 
     * @param delegate
     *            Query specification that provides the original JPA query.
     */
    public JpaQueryDecorator(JpaQuery<E> delegate) {
        this.delegate = delegate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see JpaQuery#getStrategy()
     */
    @Override
    public JpaQueryStrategy getStrategy() {
        return delegate.getStrategy();
    }

    /*
     * (non-Javadoc)
     * 
     * @see JpaQuery#getQuery(EntityManager)
     */
    @Override
    protected TypedQuery<E> getQuery(EntityManager em) {
        return delegate.getQuery(em);
    }

}
