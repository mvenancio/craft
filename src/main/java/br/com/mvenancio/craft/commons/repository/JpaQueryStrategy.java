package br.com.mvenancio.craft.commons.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class JpaQueryStrategy {

    /**
     * Executes a JPA query.
     *
     * @param em
     *            The context entity manager.
     * @param spec
     *            The query specification.
     *
     * @return The list of entities found. If no entity were found, the list will be empty.
     */
    public <E> List<E> query(EntityManager em, JpaQuery<E> spec) {
        final TypedQuery<E> query = spec.getQuery(em);
        return query.getResultList();
    }
}
