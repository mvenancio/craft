package br.com.mvenancio.craft.commons.facade;

import java.util.List;

public interface Assembler<E, J> {

    J toJson(E entity);

    List<J> toJson(List<E> entities);
}
