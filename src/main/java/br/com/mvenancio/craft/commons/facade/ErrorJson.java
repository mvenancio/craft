package br.com.mvenancio.craft.commons.facade;

public class ErrorJson extends AbstractJson {

    private String message;

    public ErrorJson() {

    }

    public ErrorJson(String message) {
        this.message = message;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
