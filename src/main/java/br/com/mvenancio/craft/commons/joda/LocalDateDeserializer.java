package br.com.mvenancio.craft.commons.joda;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;


public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DatePatterns.DATE);

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext cxt) throws IOException {
        return LocalDate.parse(jp.getValueAsString(), formatter);
    }

}
