package br.com.mvenancio.craft.commons.facade;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(Include.NON_NULL) 
public class AbstractJson {

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final ObjectWriter ow = new ObjectMapper().writer().forType(this.getClass());
        try {
            return ow.writeValueAsString(this);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

}
