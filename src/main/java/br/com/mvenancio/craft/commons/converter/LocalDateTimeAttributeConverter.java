package br.com.mvenancio.craft.commons.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDateTime localDate) {
        return (localDate == null ? null : Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant()));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date sqlDate) {
        return (sqlDate == null ? null : LocalDateTime.ofInstant(sqlDate.toInstant(), ZoneId.systemDefault()));
    }
}
