package br.com.mvenancio.craft.commons.facade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.service.CraftException;

@ControllerAdvice(annotations = RestController.class)
public class ApiErrorHandler {
	private static final Logger LOGGER = LoggerFactory.getLogger(ApiErrorHandler.class);

	@ExceptionHandler(CraftException.class)
	public ResponseEntity<?> handleBusinessException(CraftException e) {
		LOGGER.warn("[API-WARN] {}", e.getMessage());
		ResponseEntity<ErrorJson> responseEntity = new ResponseEntity<>(new ErrorJson(e.getMessage()),
				e.getHttpStatus());
		return responseEntity;
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleException(Exception e) {
		LOGGER.error("[API-ERROR] {} ", e.getMessage());
		LOGGER.error(e.getMessage(), e);
		return new ResponseEntity<>(new ErrorJson("Unexpected error occurred"),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

}