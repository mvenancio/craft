package br.com.mvenancio.craft.commons.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.service.CraftException;
import br.com.mvenancio.craft.util.ResponseUtil;

@ControllerAdvice(annotations = RestController.class)
public class CraftErrorHandler {

	@Autowired
	private ResponseUtil<AbstractJson> responseUtil;

	@ExceptionHandler(CraftException.class)
	public ResponseEntity<AbstractJson> handleCraftError(Exception exception) {
		return responseUtil.buildResponse((CraftException) exception);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<AbstractJson> handleError(Exception exception) {
		exception.printStackTrace();
		return responseUtil.buildResponse(new ErrorJson("Unexpected Error: " + exception.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
