package br.com.mvenancio.craft.commons.repository;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

public class JpaQueryLockDecorator<E> extends JpaQueryDecorator<E> {

    /**
     * Constructor
     *
     * @param delegate
     *            Query specification that provides the original JPA query.
     *
     * @see JpaQueryDecorator
     */
    public JpaQueryLockDecorator(JpaQuery<E> delegate) {
        super(delegate);
    }

    /**
     * Provides the same query from the original specification added with a "pessimistic read" locking mode.
     *
     * @see JpaQueryDecorator#getQuery(EntityManager)
     */
    @Override
    protected TypedQuery<E> getQuery(EntityManager em) {
        return super.getQuery(em).setLockMode(LockModeType.PESSIMISTIC_READ);
    }
}
