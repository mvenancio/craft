package br.com.mvenancio.craft.commons.service;

import org.springframework.http.HttpStatus;

public class CraftException extends RuntimeException {

    private static final long serialVersionUID = -1257788207444200592L;

    private final String message;

    private final HttpStatus httpStatus;

    public CraftException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    /**
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * @return the httpStatus
     */
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
