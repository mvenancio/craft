package br.com.mvenancio.craft.commons.joda;

/**
 * Centralizes date patterns to be used in the serialization/deserialization
 *
 * @author mateusv
 */
public final class DatePatterns {

    private DatePatterns() {
    }

    public static final String DATETIME = "yyyy-MM-dd HH:mm";
    public static final String DATE = "yyyy-MM-dd";

}
