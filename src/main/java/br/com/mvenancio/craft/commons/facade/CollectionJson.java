package br.com.mvenancio.craft.commons.facade;

import java.util.List;

public class CollectionJson<T> extends AbstractJson {

    private List<T> items;

    public CollectionJson() {

    }

    public CollectionJson(List<T> items) {
        this.items = items;
    }

    /**
     * @return the items
     */
    public List<T> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(List<T> items) {
        this.items = items;
    }

}
