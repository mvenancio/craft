package br.com.mvenancio.craft.commons.repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class JpaNamedQuery<E> extends JpaQuery<E> {

    private final Class<E> entityClass;
    private final String queryName;
    private final Object[] queryArgs;

    /**
     * Constructor.
     * 
     * @param entityClass
     *            The entity class returned by the query.
     * @param queryName
     *            The name of the named query.
     * @param queryArgs
     *            The query arguments, in positional order.
     */
    public JpaNamedQuery(Class<E> entityClass, String queryName, Object... queryArgs) {
        this.entityClass = entityClass;
        this.queryName = queryName;
        this.queryArgs = queryArgs;
    }

    /**
     * Creates the named query to be executed.
     * 
     * @see JpaQuery#getQuery(EntityManager)
     */
    @Override
    protected TypedQuery<E> getQuery(EntityManager em) {
        final TypedQuery<E> query = em.createNamedQuery(queryName, entityClass);
        for (int i = 0; i < queryArgs.length; i++) {
            query.setParameter(i + 1, queryArgs[i]);
        }
        return query;
    }

}
