package br.com.mvenancio.craft.commons.repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public abstract class JpaQuery<E> {

    /**
     * Provides the strategy for the execution of JPA queries.
     *
     * @see IQuery#getStrategy()
     */
    public JpaQueryStrategy getStrategy() {
        return new JpaQueryStrategy();
    }

    /**
     * Provides the JPA {@link TypedQuery} to be executed.
     *
     * @param em
     *            The context entity manager.
     */
    protected abstract TypedQuery<E> getQuery(EntityManager em);
}
