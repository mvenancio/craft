package br.com.mvenancio.craft.util;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class KeyGeneratorException extends CraftException {

	private static final long serialVersionUID = 1883558640304991531L;

	public KeyGeneratorException(String errorMessage, Exception e) {
		super(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
