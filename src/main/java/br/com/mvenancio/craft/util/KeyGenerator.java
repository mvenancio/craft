package br.com.mvenancio.craft.util;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.springframework.stereotype.Component;

/**
 * KeyGenerator: Used to generate the authentication keys of users
 *
 * @author <a href="mailto:rodrigopg@ciandt.com">Rodrigo Provinciato Gomes</a>
 */
@Component
public class KeyGenerator {

    private static final int UUID_MAX_SIZE = 16;

    /**
     * Generate a unique key with fixed size The key contains only hexadecimal
     * characters
     *
     * @return a unique key
     * @throws Exception
     */
    public String generate() {
        try {
            final javax.crypto.KeyGenerator keyGen = javax.crypto.KeyGenerator.getInstance("AES");
            keyGen.init(128);
            final SecretKey secreKey = keyGen.generateKey();
            final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secreKey);

            final String uuid = UUID.randomUUID().toString().replaceAll("-", " ").substring(0, UUID_MAX_SIZE);
            final byte[] utf8 = uuid.getBytes("UTF8");
            final byte[] resultBuffer = cipher.doFinal(utf8);

            final String key = bytesToHex(resultBuffer);
            return key;
        } catch (final IOException | GeneralSecurityException e) {
            final String errorMessage = "Erro generating key:" + e.getMessage();
            throw new KeyGeneratorException(errorMessage, e);
        }
    }

    /**
     * Convert a byte array to hex string
     *
     * @param bytes
     * @return a hex string
     */
    private static String bytesToHex(byte[] bytes) {
        final StringBuilder result = new StringBuilder();
        for (final byte byt : bytes) {
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

}