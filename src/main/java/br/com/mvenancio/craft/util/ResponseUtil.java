package br.com.mvenancio.craft.util;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.commons.facade.CollectionJson;
import br.com.mvenancio.craft.commons.facade.ErrorJson;
import br.com.mvenancio.craft.commons.service.CraftException;

@Component
public class ResponseUtil<T extends AbstractJson> {

    public ResponseEntity<AbstractJson> buildResponse(final T body) {
        HttpStatus httpStatus = HttpStatus.OK;
        if (body == null) {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return buildResponse(body, httpStatus);
    }

    public ResponseEntity<AbstractJson> buildResponse(final T body, HttpStatus httpStatus) {
        return new ResponseEntity<>(body, httpStatus);
    }

    public ResponseEntity<?> buildResponse(final Object any, HttpStatus httpStatus) {
        return new ResponseEntity<>(any, httpStatus);
    }

    public ResponseEntity<AbstractJson> buildResponse(List<T> body) {
        HttpStatus httpStatus = HttpStatus.OK;
        if (body == null) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (body.isEmpty()) {
            httpStatus = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(new CollectionJson<T>(body), httpStatus);
    }

    public ResponseEntity<AbstractJson> buildResponse(CollectionJson<T> body) {
        HttpStatus httpStatus = HttpStatus.OK;
        if (body == null) {
            httpStatus = HttpStatus.NOT_FOUND;
        } else if (body.getItems().isEmpty()) {
            httpStatus = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(body, httpStatus);
    }

    public ResponseEntity<AbstractJson> buildResponse(CraftException exception) {
        return new ResponseEntity<>(new ErrorJson(exception.getMessage()), exception.getHttpStatus());
    }

}
