package br.com.mvenancio.craft.util;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class MailUtil {
	private static final Logger LOGGER  = LoggerFactory.getLogger(MailUtil.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${mail.from}")
	private String from;

	public void sendEmail(String email, String link) throws MessagingException {
		LOGGER.info("[MAIL] Sending activate email for {}",email);
		MimeMessage message = javaMailSender.createMimeMessage();

		// use the true flag to indicate you need a multipart message
		MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
		helper.setTo(email);
		helper.setFrom(from);
		helper.setSubject("Ativação cadastro Sr. Artesão");

		// use the true flag to indicate the text included is HTML
		helper.setText(
				"<html><body><h1>Bem vindo ao Sr. Artesão</h1><p>Confirme seu cadastro clicando no link abaixo</p>"
						+ "<p><a href=\"" + link + "\" >Clique para ativar</a></p></body></html>",
				true);
		javaMailSender.send(message);
		LOGGER.info("[MAIL] Sent activate email");
	}
	
	public void sendForgotEmail(String email, String link) throws MessagingException {
		LOGGER.info("[MAIL] Sending recovery password email for {}",email);
		MimeMessage message = javaMailSender.createMimeMessage();

		// use the true flag to indicate you need a multipart message
		MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
		helper.setTo(email);
		helper.setFrom(from);
		helper.setSubject("Alteração de senha Sr. Artesão");

		// use the true flag to indicate the text included is HTML
		helper.setText(
				"<html><body><h1></h1><p>Clique no link abaixo para redefinir sua senha</p>"
						+ "<p><a href=\"" + link + "\" >"+link+"</a></p></body></html>",
				true);
		javaMailSender.send(message);
		LOGGER.info("[MAIL] Sent recovery password email");
	}
}
