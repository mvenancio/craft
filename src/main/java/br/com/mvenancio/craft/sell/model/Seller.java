package br.com.mvenancio.craft.sell.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Table(name = "seller")
@Entity
@NamedQuery(name=Seller.FIND_BY_USER, query="SELECT s FROM Seller s WHERE s.user = ?1 ORDER BY s.id")
public class Seller {

    private static final String SELLER_ID_SEQ = "seller_id_seq";
    public static final String FIND_BY_USER = "Seller-FindByUser";

    @Id
    @SequenceGenerator(name = SELLER_ID_SEQ, sequenceName = SELLER_ID_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SELLER_ID_SEQ)
    private Long id;

    private String name;

    private BigDecimal percentage;

    @OneToOne
    @JoinColumn(name = "craft_user_id")
    private User user;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the percentage
     */
    public BigDecimal getPercentage() {
        return percentage;
    }

    /**
     * @param percentage
     *            the percentage to set
     */
    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

}
