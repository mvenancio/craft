package br.com.mvenancio.craft.sell.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.sell.model.Sale;

@Repository
public class SaleRepository extends JpaRepository<Sale, Long> {

	@Override
	protected Class<Sale> getEntityClass() {
		return Sale.class;
	}
	
	public List<Sale> findByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Sale.FIND_BY_USER, user));
    }
	
	public List<Sale> findEstimatesByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Sale.FIND_ESTIMATES_BY_USER, user));
    }
	
	public List<Sale> findFinishedByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Sale.FIND_FINISHEDS_BY_USER, user));
    }

}
