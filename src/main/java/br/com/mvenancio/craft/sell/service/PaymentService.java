package br.com.mvenancio.craft.sell.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.sell.model.Payment;
import br.com.mvenancio.craft.sell.model.PaymentStatus;
import br.com.mvenancio.craft.sell.model.Sale;
import br.com.mvenancio.craft.sell.repository.PaymentRepository;

@Service
public class PaymentService {

	@Autowired
	private PaymentRepository repository;

	@Autowired
	private UserService userService;

	@Autowired
	private SaleService saleService;

	@Transactional(propagation = Propagation.REQUIRED)
	public void createPayments(Sale sale, User user) {
		BigDecimal installmentValue = sale.getTotalValue().divide(BigDecimal.valueOf(sale.getInstallments()),
				RoundingMode.HALF_UP);
		for (int i = 0; i < sale.getInstallments(); i++) {
			Payment payment = new Payment();
			payment.setSale(sale);
			payment.setDueDate(sale.getFirstDueDate().plusMonths(i));
			payment.setStatus(PaymentStatus.PENDING);
			payment.setTotalValue(installmentValue);
			payment.setUser(user);
			repository.add(payment);
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Payment> findByUser(String encodedUserId) {
		return repository.findByUser(userService.findByToken(encodedUserId));
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Payment> findExpiredPayments(String encodedUserId) {
		return repository.findExpiredPaymentsBy(userService.findByToken(encodedUserId), LocalDate.now());
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void cancelPayments(Sale sale) {
		if (sale.getPayments() == null) {
			return;
		}
		sale.getPayments().forEach(payment -> {
			payment.setStatus(PaymentStatus.CANCELLED);
			repository.update(payment);
		});
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Payment pay(Long paymentId, String encodedUserId) {
		Payment payment = repository.get(paymentId);
		User user = userService.findByToken(encodedUserId);
		if (payment == null | !payment.getUser().getId().equals(user.getId())) {
			throw new PaymentNotFoundException(paymentId);
		}
		payment.setStatus(PaymentStatus.PAID);
		payment.setPaymentDate(LocalDate.now());
		repository.update(payment);

		saleService.verifyAndFinish(payment.getSale().getId(), encodedUserId);
		return payment;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Payment cancel(Long paymentId, String encodedUserId) {
		Payment payment = repository.get(paymentId);
		User user = userService.findByToken(encodedUserId);
		if (payment == null | !payment.getUser().getId().equals(user.getId())) {
			throw new PaymentNotFoundException(paymentId);
		}
		payment.setStatus(PaymentStatus.CANCELLED);
		repository.update(payment);
		saleService.verifyAndFinish(payment.getSale().getId(), encodedUserId);
		return payment;
	}

	public BigDecimal getTotalPaid(String encodedUserId) {
		BigDecimal totalCost = repository.getTotalPaid(userService.findByToken(encodedUserId));
		return totalCost == null ? BigDecimal.ZERO : totalCost;
	}

}
