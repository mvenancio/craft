package br.com.mvenancio.craft.sell.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.sell.model.Seller;

@Component
public class SellerAssembler implements Assembler<Seller, SellerJson> {

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public SellerJson toJson(Seller entity) {
        SellerJson json = new SellerJson();
        json.setId(entity.getId());
        json.setName(entity.getName());
        json.setPercentage(entity.getPercentage());
        return json;
    }

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<SellerJson> toJson(List<Seller> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }

}
