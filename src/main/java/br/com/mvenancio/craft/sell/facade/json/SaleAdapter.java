package br.com.mvenancio.craft.sell.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.sell.model.Sale;

@Component
public class SaleAdapter implements Assembler<Sale, SaleResponse> {

	@Autowired
	private CustomerAssembler customerAdapter;

	@Autowired
	private SellerAssembler sellerAdapter;

	@Override
	public SaleResponse toJson(Sale entity) {
		SaleResponse json = new SaleResponse();
		json.setId(entity.getId());
		json.setCustomer(customerAdapter.toJson(entity.getCustomer()));
		if(entity.getSeller()!=null){
			json.setSeller(sellerAdapter.toJson(entity.getSeller()));
		}
		json.setTotalValue(entity.getTotalValue());
		json.setStatus(entity.getStatus());
		json.setDiscountValue(entity.getDiscountValue());
		json.setDiscountPercentage(entity.getDiscountPercentage());
		json.setFirstDueDate(entity.getFirstDueDate());
		json.setDeliveryDate(entity.getDeliveryDate());
		json.setInstallments(entity.getInstallments());
		json.setObservation(entity.getObservation());
		json.setCreationTime(entity.getCreationTime());
		json.setApprovedTime(entity.getApprovedTime());
		json.setPartyDate(entity.getPartyDate());
		json.setWorkValue(entity.getWorkValue());
		json.setTotalCostsValue(entity.getTotalCostsValue());
		json.setProfitValue(entity.getProfitValue());
		json.setRateValue(entity.getRateValue());
		json.setItems(entity.getItems().stream().map(item -> {
			ItemResponse itemResponse = new ItemResponse();
			itemResponse.setProductDescription(item.getProduct().getDescription());
			itemResponse.setProductId(item.getProduct().getId());
			itemResponse.setDescription(item.getDescription());
			itemResponse.setTotalValue(item.getValue());
			itemResponse.setAmount(item.getAmount());
			return itemResponse;
		}).collect(Collectors.toList()));
		return json;
	}

	@Override
	public List<SaleResponse> toJson(List<Sale> entities) {
		return entities.stream().map(item -> toJson(item)).collect(Collectors.toList());
	}

}
