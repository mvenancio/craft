package br.com.mvenancio.craft.sell.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.sell.model.Payment;

@Repository
public class PaymentRepository extends JpaRepository<Payment, Long> {

	@Override
	protected Class<Payment> getEntityClass() {
		return Payment.class;
	}

	public List<Payment> findByUser(User user) {
		return this.query(new JpaNamedQuery<>(getEntityClass(), Payment.FIND_BY_USER, user));
	}
	
	public List<Payment> findExpiredPaymentsBy(User user, LocalDate dueDate) {
		return this.query(new JpaNamedQuery<>(getEntityClass(), Payment.FIND_EXPIRED_PAYMENTS, user, dueDate));
	}

	public BigDecimal getTotalPaid(User user) {
		LocalDate now = LocalDate.now();
		TypedQuery<BigDecimal> query = em.createNamedQuery(Payment.GET_TOTAL_PAID, BigDecimal.class);
		query.setParameter(1, user);
		query.setParameter(2, now.withDayOfMonth(1));
		query.setParameter(3, now.withDayOfMonth(1).plusMonths(1).minusDays(1));
		return query.getSingleResult();
	}

}
