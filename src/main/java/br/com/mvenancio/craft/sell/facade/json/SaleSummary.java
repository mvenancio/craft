package br.com.mvenancio.craft.sell.facade.json;

import java.math.BigDecimal;

public class SaleSummary {

	public SaleSummary() {
	}

	public SaleSummary(String monthYear, BigDecimal workValue, BigDecimal profitValue, BigDecimal totalCostsValue,
			BigDecimal rateValue, BigDecimal totalValue) {
		this.monthYear = monthYear;
		this.profitValue = profitValue;
		this.rateValue = rateValue;
		this.totalCostsValue = totalCostsValue;
		this.workValue = workValue;
		this.totalValue = totalValue;
	}

	private String monthYear;
	private BigDecimal workValue;
	private BigDecimal profitValue;
	private BigDecimal totalCostsValue;
	private BigDecimal rateValue;
	private BigDecimal totalValue;

	public String getMonthYear() {
		return monthYear;
	}

	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}

	public BigDecimal getWorkValue() {
		return workValue;
	}

	public void setWorkValue(BigDecimal workValue) {
		this.workValue = workValue;
	}

	public BigDecimal getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(BigDecimal profitValue) {
		this.profitValue = profitValue;
	}

	public BigDecimal getTotalCostsValue() {
		return totalCostsValue;
	}

	public void setTotalCostsValue(BigDecimal totalCostsValue) {
		this.totalCostsValue = totalCostsValue;
	}

	public BigDecimal getRateValue() {
		return rateValue;
	}

	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

}