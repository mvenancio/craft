package br.com.mvenancio.craft.sell.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.sell.model.Customer;

@Repository
public class CustomerRepository extends JpaRepository<Customer, Long> {

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.repository.JpaRepository#getEntityClass()
     */
    @Override
    protected Class<Customer> getEntityClass() {
        return Customer.class;
    }

    public List<Customer> findByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Customer.FIND_BY_USER, user));
    }

}
