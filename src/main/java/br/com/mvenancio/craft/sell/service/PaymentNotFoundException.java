package br.com.mvenancio.craft.sell.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class PaymentNotFoundException extends CraftException {

	private static final long serialVersionUID = -7152439083816376546L;

	public PaymentNotFoundException(Long id) {
		super("Payment not found: " + id, HttpStatus.NOT_FOUND);
	}

}
