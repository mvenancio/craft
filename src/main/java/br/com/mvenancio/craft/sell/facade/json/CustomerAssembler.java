package br.com.mvenancio.craft.sell.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.sell.model.Customer;

@Component
public class CustomerAssembler implements Assembler<Customer, CustomerJson> {

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public CustomerJson toJson(Customer entity) {
        CustomerJson customer = new CustomerJson();
        customer.setId(entity.getId());
        customer.setAddress(entity.getAddress());
        customer.setBirth(entity.getBirth());
        customer.setCity(entity.getCity());
        customer.setComplement(entity.getComplement());
        customer.setDocument(entity.getDocument());
        customer.setDocument2(entity.getDocument2());
        customer.setEmail(entity.getEmail());
        customer.setMobilePhone(entity.getMobilePhone());
        customer.setName(entity.getName());
        customer.setNeighborhood(entity.getNeighborhood());
        customer.setNumber(entity.getNumber());
        customer.setObservation(entity.getObservation());
        customer.setPhone(entity.getPhone());
        customer.setPostalCode(entity.getPostalCode());
        customer.setState(entity.getState());
        customer.setFacebook(entity.getFacebook());
        customer.setGooglePlus(entity.getGooglePlus());
        customer.setTwitter(entity.getTwitter());
        customer.setOther(entity.getOther());
        return customer;
    }

    public Customer toEntity(CustomerJson json) {
        Customer customer = new Customer();
        customer.setAddress(json.getAddress());
        customer.setBirth(json.getBirth());
        customer.setCity(json.getCity());
        customer.setComplement(json.getComplement());
        customer.setDocument(json.getDocument());
        customer.setDocument2(json.getDocument2());
        customer.setEmail(json.getEmail());
        customer.setMobilePhone(json.getMobilePhone());
        customer.setName(json.getName());
        customer.setNeighborhood(json.getNeighborhood());
        customer.setNumber(json.getNumber());
        customer.setObservation(json.getObservation());
        customer.setPhone(json.getPhone());
        customer.setPostalCode(json.getPostalCode());
        customer.setState(json.getState());
        customer.setFacebook(json.getFacebook());
        customer.setGooglePlus(json.getGooglePlus());
        customer.setTwitter(json.getTwitter());
        customer.setOther(json.getOther());
        return customer;
    }

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<CustomerJson> toJson(List<Customer> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }

}
