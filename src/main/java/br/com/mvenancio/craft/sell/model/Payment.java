package br.com.mvenancio.craft.sell.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Entity
@Table(name = "payment")
@NamedQueries({
		@NamedQuery(name = Payment.FIND_BY_USER, query = "SELECT p FROM Payment p WHERE p.user = ?1 ORDER BY p.id "),
		@NamedQuery(name = Payment.GET_TOTAL_PAID, query = "SELECT SUM(p.totalValue) FROM Payment p WHERE p.user = ?1 "
				+ " AND p.status = 'PAID' AND p.paymentDate between ?2 AND ?3"),
		@NamedQuery(name = Payment.FIND_EXPIRED_PAYMENTS, query = "SELECT p FROM Payment p WHERE p.user = ?1 "
				+ " AND p.status = 'PENDING' AND p.dueDate <= ?2")})
public class Payment {

	private static final String ID_SEQ = "payment_id_seq";

	public static final String FIND_BY_USER = "Payment-FindByUser";
	public static final String GET_TOTAL_PAID = "Payment-GetTotalPaid";
	public static final String FIND_EXPIRED_PAYMENTS = "Payment-FindExpiredPayments";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	@Enumerated(EnumType.STRING)
	private PaymentStatus status;

	@ManyToOne
	@JoinColumn(name = "sale_id")
	private Sale sale;

	@Column(name = "due_date")
	private LocalDate dueDate;

	@Column(name = "payment_date")
	private LocalDate paymentDate;

	@Column(name = "total_value")
	private BigDecimal totalValue;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

}
