package br.com.mvenancio.craft.sell.facade;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.sell.facade.json.SellerAssembler;
import br.com.mvenancio.craft.sell.facade.json.SellerJson;
import br.com.mvenancio.craft.sell.service.SellerService;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/sellers")
public class SellerFacade {

    @Autowired
    private ResponseUtil<SellerJson> responseUtil;

    @Autowired
    private SellerAssembler assembler;

    @Autowired
    private SellerService service;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> findAll(@NotEmpty @RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.findAll(encodedUserId)));
    }

    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> post(@Validated @RequestBody SellerJson request, @RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.add(request.getName(), request.getPercentage(),
                encodedUserId)));
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> put(@PathVariable("id") @Validated @NotNull Long id,
            @Validated @RequestBody SellerJson request, @RequestHeader(
                    value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.update(id, request.getName(), request
                .getPercentage(), encodedUserId)));
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> get(@PathVariable("id") @NotNull Long userId, @RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.get(userId, encodedUserId)));
    }

}
