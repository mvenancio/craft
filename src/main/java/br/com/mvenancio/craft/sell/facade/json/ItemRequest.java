package br.com.mvenancio.craft.sell.facade.json;

import java.math.BigDecimal;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class ItemRequest extends AbstractJson {
	private Long productId;
	private BigDecimal amount;
	private BigDecimal totalValue;
	private BigDecimal unitValue;
	private String description;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getUnitValue() {
		return unitValue;
	}

	public void setUnitValue(BigDecimal unitValue) {
		this.unitValue = unitValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
