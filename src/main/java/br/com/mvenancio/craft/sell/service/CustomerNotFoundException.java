package br.com.mvenancio.craft.sell.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class CustomerNotFoundException extends CraftException {

    private static final long serialVersionUID = 5433308814271414743L;

    public CustomerNotFoundException(Long id) {
        super("Customer not found: "+id, HttpStatus.NOT_FOUND);
    }

}
