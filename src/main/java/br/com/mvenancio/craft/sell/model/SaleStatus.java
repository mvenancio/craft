package br.com.mvenancio.craft.sell.model;

public enum SaleStatus {
	
	ESTIMATE, EXPIRED, CANCELLED, APPROVED, FINISHED;

}
