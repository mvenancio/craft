package br.com.mvenancio.craft.sell.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.sell.facade.json.PaymentAdapter;
import br.com.mvenancio.craft.sell.facade.json.PaymentResponse;
import br.com.mvenancio.craft.sell.service.PaymentService;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/payments")
public class PaymentFacade {

	@Autowired
	private ResponseUtil<PaymentResponse> responseUtil;

	@Autowired
	private PaymentAdapter adapter;

	@Autowired
	private PaymentService service;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> get(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.findByUser(encodedUserId)));
	}

	@RequestMapping(value = "/expired", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> getExpired(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.findExpiredPayments(encodedUserId)));
	}

	@RequestMapping(value = "/{id}/pay", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> pay(@PathVariable("id") Long paymentId,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.pay(paymentId, encodedUserId)));
	}

	@RequestMapping(value = "/{id}/cancel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> cancel(@PathVariable("id") Long paymentId,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(service.cancel(paymentId, encodedUserId)));
	}

	@RequestMapping(value = "/total-paid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTotalPaid(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(service.getTotalPaid(encodedUserId), HttpStatus.OK);
	}

}
