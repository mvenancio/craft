package br.com.mvenancio.craft.sell.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.sell.model.Seller;

@Repository
public class SellerRepository extends JpaRepository<Seller, Long> {

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.repository.JpaRepository#getEntityClass()
     */
    @Override
    protected Class<Seller> getEntityClass() {
        return Seller.class;
    }

    public List<Seller> findByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Seller.FIND_BY_USER, user));
    }


}
