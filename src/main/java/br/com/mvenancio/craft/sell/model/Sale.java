package br.com.mvenancio.craft.sell.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Entity
@Table(name = "sale")
@NamedQueries({
		@NamedQuery(name = Sale.FIND_BY_USER, query = "SELECT s FROM Sale s WHERE s.user = ?1 ORDER BY s.deliveryDate"),
		@NamedQuery(name = Sale.FIND_ESTIMATES_BY_USER, query = "SELECT s FROM Sale s WHERE s.user = ?1 AND s.status = 'ESTIMATE' ORDER BY s.creationTime"),
		@NamedQuery(name = Sale.FIND_FINISHEDS_BY_USER, query = "SELECT s FROM Sale s WHERE s.user = ?1 AND s.status = 'FINISHED' ORDER BY s.creationTime")})
public class Sale {

	private static final String ID_SEQ = "sale_id_seq";
	public static final String FIND_BY_USER = "Sale-FindByUser";
	public static final String FIND_ESTIMATES_BY_USER = "Sale-FindEstimatesByUser";
	public static final String FIND_FINISHEDS_BY_USER = "Sale-FindFinishedsByUser";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	@OneToOne
	@JoinColumn(name = "seller_id")
	private Seller seller;

	@Enumerated(EnumType.STRING)
	private SaleStatus status;

	@OneToMany(mappedBy = "sale", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SaleItem> items;

	@Column(name = "total_value")
	private BigDecimal totalValue = BigDecimal.ZERO;

	@Column(name = "discount_value")
	private BigDecimal discountValue = BigDecimal.ZERO;

	@Column(name = "discount_percentage")
	private BigDecimal discountPercentage = BigDecimal.ZERO;

	@Column(name = "creation_time")
	private LocalDateTime creationTime;

	@Column(name = "approved_time")
	private LocalDateTime approvedTime;

	@Column(name = "delivery_date")
	private LocalDate deliveryDate;

	@Column(name = "first_due_date")
	private LocalDate firstDueDate;

	@Column(name = "party_date")
	private LocalDate partyDate;

	private Integer installments;

	@Column(name = "work_value")
	private BigDecimal workValue = BigDecimal.ZERO;

	@Column(name = "profit_value")
	private BigDecimal profitValue = BigDecimal.ZERO;

	@Column(name = "total_costs_value")
	private BigDecimal totalCostsValue = BigDecimal.ZERO;

	@Column(name = "rate_value")
	private BigDecimal rateValue = BigDecimal.ZERO;
	
	private String observation;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;

	@OneToMany(mappedBy = "sale", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Payment> payments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public SaleStatus getStatus() {
		return status;
	}

	public void setStatus(SaleStatus status) {
		this.status = status;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<SaleItem> getItems() {
		return items;
	}

	public void setItems(List<SaleItem> items) {
		this.items = items;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public LocalDate getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(LocalDate firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(LocalDateTime creationTime) {
		this.creationTime = creationTime;
	}

	public LocalDateTime getApprovedTime() {
		return approvedTime;
	}

	public void setApprovedTime(LocalDateTime approvedTime) {
		this.approvedTime = approvedTime;
	}

	public LocalDate getPartyDate() {
		return partyDate;
	}

	public void setPartyDate(LocalDate partyDate) {
		this.partyDate = partyDate;
	}

	public BigDecimal getWorkValue() {
		return workValue;
	}

	public void setWorkValue(BigDecimal workValue) {
		this.workValue = workValue;
	}

	public BigDecimal getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(BigDecimal profitValue) {
		this.profitValue = profitValue;
	}

	public BigDecimal getTotalCostsValue() {
		return totalCostsValue;
	}

	public void setTotalCostsValue(BigDecimal totalCostsValue) {
		this.totalCostsValue = totalCostsValue;
	}

	public BigDecimal getRateValue() {
		return rateValue;
	}

	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}


}
