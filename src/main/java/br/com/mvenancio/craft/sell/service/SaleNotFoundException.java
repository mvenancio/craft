package br.com.mvenancio.craft.sell.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class SaleNotFoundException extends CraftException {

    private static final long serialVersionUID = 5433308814271414743L;

    public SaleNotFoundException(Long id) {
        super("Sale not found: "+id, HttpStatus.NOT_FOUND);
    }

}
