package br.com.mvenancio.craft.sell.model;

public enum PaymentStatus {

	PENDING, PAID, CANCELLED;

}
