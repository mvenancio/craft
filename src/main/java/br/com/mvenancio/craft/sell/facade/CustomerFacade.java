package br.com.mvenancio.craft.sell.facade;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.sell.facade.json.CustomerAssembler;
import br.com.mvenancio.craft.sell.facade.json.CustomerJson;
import br.com.mvenancio.craft.sell.service.CustomerService;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/customers")
public class CustomerFacade {

    @Autowired
    private ResponseUtil<CustomerJson> responseUtil;

    @Autowired
    private CustomerAssembler assembler;

    @Autowired
    private CustomerService service;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> findAll(@RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.findAll(encodedUserId)));
    }

    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> post(@Validated @RequestBody CustomerJson request, @RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.add(assembler.toEntity(request), encodedUserId)));
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> put(@PathVariable("id") @NotNull Long id,
            @Validated @RequestBody CustomerJson request, @RequestHeader(
                    value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.update(id, assembler.toEntity(request),
                encodedUserId)));
    }

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AbstractJson> get(@PathVariable("id") @NotNull Long userId, @RequestHeader(
            value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
        return responseUtil.buildResponse(assembler.toJson(service.get(userId, encodedUserId)));
    }

}
