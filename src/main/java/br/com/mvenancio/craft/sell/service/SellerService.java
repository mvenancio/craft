package br.com.mvenancio.craft.sell.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.sell.model.Seller;
import br.com.mvenancio.craft.sell.repository.SellerRepository;

@Service
public class SellerService {

    @Autowired
    private SellerRepository repository;

    @Autowired
    private UserService userService;


    @Transactional(propagation=Propagation.REQUIRED)
    public Seller add(String name, BigDecimal percentage, String encodedUserId) {
        Seller entity = new Seller();
        entity.setName(name);
        entity.setPercentage(percentage);
        entity.setUser(userService.findByToken(encodedUserId));
        return repository.add(entity );
    }

    @Transactional(propagation=Propagation.REQUIRED)
    public Seller update(Long id, String name, BigDecimal percentage, String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        Seller entity = repository.get(id);
        if (entity == null || !entity.getUser().equals(user)) {
            throw new SellerNotFoundException(id);
        }
        entity.setName(name);
        entity.setPercentage(percentage);
        return repository.update(entity );
    }

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public Seller get(Long id, String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        Seller entity = repository.get(id);
        if (entity == null || !entity.getUser().equals(user)) {
            throw new SellerNotFoundException(id);
        }
        return entity;
    }

    @Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
    public List<Seller> findAll(String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        return repository.findByUser(user);

    }

}
