package br.com.mvenancio.craft.sell.facade;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.sell.facade.json.PaymentAdapter;
import br.com.mvenancio.craft.sell.facade.json.SaleAdapter;
import br.com.mvenancio.craft.sell.facade.json.SaleRequest;
import br.com.mvenancio.craft.sell.facade.json.SaleSummary;
import br.com.mvenancio.craft.sell.model.Sale;
import br.com.mvenancio.craft.sell.service.SaleService;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/sales")
public class SaleFacade {

	@Autowired
	private ResponseUtil responseUtil;

	@Autowired
	private SaleAdapter adapter;

	@Autowired
	private PaymentAdapter paymentAdapter;

	@Autowired
	private SaleService saleService;

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@Validated @RequestBody SaleRequest request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.add(request, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> put(@PathVariable("id") @Validated @NotNull Long id,
			@Validated @RequestBody SaleRequest request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.update(id, request, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> getById(@PathVariable("id") @Validated @NotNull Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.get(id, encodedUserId)));
	}

	@RequestMapping(value = "/{id}/approve", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> approve(@PathVariable("id") @Validated @NotNull Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.approve(id, encodedUserId)));
	}

	@RequestMapping(value = "/{id}/cancel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> cancel(@PathVariable("id") @Validated @NotNull Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.cancel(id, encodedUserId)));
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> get(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.findByUser(encodedUserId)));
	}

	@RequestMapping(value = "/estimates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> getEstimates(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toJson(saleService.findEstimatesByUser(encodedUserId)));
	}

	@RequestMapping(value = "/finisheds/summary", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, SaleSummary> getSaleSummary(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return saleService.findFinishedByUser(encodedUserId);
	}

	@RequestMapping(value = "/{id}/payments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> getPayments(@PathVariable("id") Long saleId,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		Sale sale = saleService.get(saleId, encodedUserId);
		return responseUtil.buildResponse(paymentAdapter.toJson(sale.getPayments()));
	}

}
