package br.com.mvenancio.craft.sell.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;

import br.com.mvenancio.craft.production.service.ProductService;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.sell.facade.json.SaleRequest;
import br.com.mvenancio.craft.sell.facade.json.SaleSummary;
import br.com.mvenancio.craft.sell.model.PaymentStatus;
import br.com.mvenancio.craft.sell.model.Sale;
import br.com.mvenancio.craft.sell.model.SaleItem;
import br.com.mvenancio.craft.sell.model.SaleStatus;
import br.com.mvenancio.craft.sell.repository.SaleRepository;

@Service
public class SaleService {

	@Autowired
	private SaleRepository repository;

	@Autowired
	private UserService userService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SellerService sellerService;

	@Autowired
	private ProductService productService;

	@Autowired
	private PaymentService paymentService;

	@Transactional(propagation = Propagation.REQUIRED)
	public Sale add(SaleRequest saleRequest, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);

		Sale sale = new Sale();
		fillData(saleRequest, encodedUserId, sale);
		sale.setUser(user);
		sale.setCreationTime(LocalDateTime.now());
		return repository.add(sale);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Sale update(Long id, SaleRequest saleRequest, String encodedUserId) {
		Sale sale = get(id, encodedUserId);
		fillData(saleRequest, encodedUserId, sale);
		return repository.update(sale);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Sale get(Long id, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Sale sale = repository.get(id);
		if (sale == null || !sale.getUser().getId().equals(user.getId())) {
			throw new SaleNotFoundException(id);
		}
		return sale;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Sale approve(Long id, String encodedUserId) {
		Sale sale = get(id, encodedUserId);
		sale.setStatus(SaleStatus.APPROVED);
		sale.setApprovedTime(LocalDateTime.now());
		repository.update(sale);
		paymentService.createPayments(sale, userService.findByToken(encodedUserId));
		return sale;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Sale verifyAndFinish(Long id, String encodedUserId) {
		Sale sale = get(id, encodedUserId);
		if (SaleStatus.APPROVED == sale.getStatus()) {
			if (!sale.getPayments().stream().filter(payment -> PaymentStatus.PENDING == payment.getStatus()).findFirst()
					.isPresent()) {
				sale.setStatus(SaleStatus.FINISHED);
				repository.update(sale);
			}
		}
		return sale;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Sale cancel(Long id, String encodedUserId) {
		Sale sale = get(id, encodedUserId);
		sale.setStatus(SaleStatus.CANCELLED);
		repository.update(sale);
		paymentService.cancelPayments(sale);
		return sale;
	}

	private void fillData(SaleRequest saleRequest, String encodedUserId, Sale sale) {
		sale.setCustomer(customerService.get(saleRequest.getCustomerId(), encodedUserId));
		if (saleRequest.getSellerId()!= null) {
			sale.setSeller(sellerService.get(saleRequest.getSellerId(), encodedUserId));
		}
		sale.setStatus(SaleStatus.ESTIMATE);
		sale.setTotalValue(saleRequest.getTotalValue());
		sale.setDeliveryDate(saleRequest.getDeliveryDate());
		sale.setFirstDueDate(saleRequest.getFirstDueDate());
		sale.setInstallments(saleRequest.getInstallments());
		sale.setObservation(saleRequest.getObservation());
		sale.setDiscountPercentage(saleRequest.getDiscountPercentage());
		sale.setDiscountValue(saleRequest.getDiscountValue());
		sale.setPartyDate(saleRequest.getPartyDate());
		sale.setWorkValue(saleRequest.getWorkValue());
		sale.setTotalCostsValue(saleRequest.getTotalCostsValue());
		sale.setProfitValue(saleRequest.getProfitValue());
		sale.setRateValue(saleRequest.getRateValue());
		if (sale.getItems() != null) {
			sale.getItems().clear();
		} else {
			sale.setItems(new ArrayList<>());
		}
		sale.getItems().addAll(saleRequest.getItems().stream().map(item -> {
			SaleItem saleItem = new SaleItem();
			saleItem.setProduct(productService.get(item.getProductId(), encodedUserId));
			saleItem.setProductDescription(saleItem.getProduct().getDescription());
			saleItem.setSale(sale);
			saleItem.setAmount(item.getAmount());
			saleItem.setValue(item.getTotalValue());
			saleItem.setUnitValue(item.getUnitValue());
			saleItem.setDescription(item.getDescription());
			return saleItem;
		}).collect(Collectors.toList()));
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Sale> findByUser(String encodedUserId) {
		return repository.findByUser(userService.findByToken(encodedUserId));
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Sale> findEstimatesByUser(String encodedUserId) {
		return repository.findEstimatesByUser(userService.findByToken(encodedUserId));
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Map<String, SaleSummary> findFinishedByUser(String encodedUserId) {
		List<Sale> sales = repository.findFinishedByUser(userService.findByToken(encodedUserId));
		Map<String, List<Sale>> index = sales.stream().collect(
				Collectors.groupingBy((sale) -> sale.getApprovedTime().format(DateTimeFormatter.ofPattern("yyyy-MM"))));

		Map<String, SaleSummary> result = new TreeMap<>();
		for (Entry<String, List<Sale>> entry : index.entrySet()) {
			Optional<SaleSummary> map = entry.getValue().stream().reduce((sale1, sale2) -> {
				sale1.setProfitValue(getValidValue(sale1.getProfitValue()).add(getValidValue(sale2.getProfitValue())));
				sale1.setRateValue(getValidValue(sale1.getRateValue()).add(getValidValue(sale2.getRateValue())));
				sale1.setTotalCostsValue(
						getValidValue(sale1.getTotalCostsValue()).add(getValidValue(sale2.getTotalCostsValue())));
				sale1.setWorkValue(getValidValue(sale1.getWorkValue()).add(getValidValue(sale2.getWorkValue())));
				sale1.setTotalValue(getValidValue(sale1.getTotalValue()).add(getValidValue(sale2.getTotalValue())));
				return sale1;
			}).map(sale -> {
				return new SaleSummary(entry.getKey(), sale.getWorkValue(), sale.getProfitValue(),
						sale.getTotalCostsValue(), sale.getRateValue(), sale.getTotalValue());
			});
			result.put(entry.getKey(), map.get());
		}

		return result;
	}

	private BigDecimal getValidValue(BigDecimal value) {
		return Objects.firstNonNull(value, BigDecimal.ZERO);
	}
}
