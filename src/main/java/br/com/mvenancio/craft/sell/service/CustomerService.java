package br.com.mvenancio.craft.sell.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.sell.model.Customer;
import br.com.mvenancio.craft.sell.repository.CustomerRepository;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private UserService userService;

    @Transactional(propagation = Propagation.REQUIRED)
    public Customer add(Customer customer, String encodedId) {
        User user = userService.findByToken(encodedId);
        customer.setUser(user);
        return repository.add(customer);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Customer update(Long id, Customer json, String encodedId) {
        User user = userService.findByToken(encodedId);
        Customer customer = repository.get(id);
        if (customer == null || !customer.getUser().equals(user)) {
            throw new CustomerNotFoundException(id);
        }

        customer.setAddress(json.getAddress());
        customer.setBirth(json.getBirth());
        customer.setCity(json.getCity());
        customer.setComplement(json.getComplement());
        customer.setDocument(json.getDocument());
        customer.setDocument2(json.getDocument2());
        customer.setEmail(json.getEmail());
        customer.setMobilePhone(json.getMobilePhone());
        customer.setName(json.getName());
        customer.setNeighborhood(json.getNeighborhood());
        customer.setNumber(json.getNumber());
        customer.setObservation(json.getObservation());
        customer.setPhone(json.getPhone());
        customer.setPostalCode(json.getPostalCode());
        customer.setState(json.getState());
        customer.setFacebook(json.getFacebook());
        customer.setGooglePlus(json.getGooglePlus());
        customer.setTwitter(json.getTwitter());
        customer.setOther(json.getOther());
        
        return repository.update(customer);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public Customer get(Long id, String encodedId) {
        User user = userService.findByToken(encodedId);
        Customer customer = repository.get(id);
        if (customer == null || !customer.getUser().equals(user)) {
            throw new CustomerNotFoundException(id);
        }
        return customer;
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Customer> findAll(String encodedId) {
        User user = userService.findByToken(encodedId);
        return repository.findByUser(user);

    }

}
