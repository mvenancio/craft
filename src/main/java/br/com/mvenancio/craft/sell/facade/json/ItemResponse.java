package br.com.mvenancio.craft.sell.facade.json;

import java.math.BigDecimal;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class ItemResponse extends AbstractJson {

	private Long productId;
	private String productDescription;
	private BigDecimal totalValue;
	private BigDecimal amount;
	private String description;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
