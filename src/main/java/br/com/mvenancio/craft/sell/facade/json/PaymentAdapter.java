package br.com.mvenancio.craft.sell.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.sell.model.Payment;

@Component
public class PaymentAdapter implements Assembler<Payment, PaymentResponse> {

	@Autowired
	private SaleAdapter saleAdapter;

	@Override
	public PaymentResponse toJson(Payment entity) {
		PaymentResponse response = new PaymentResponse();
		response.setId(entity.getId());
		response.setDueDate(entity.getDueDate());
		response.setPaymentDate(entity.getPaymentDate());
		response.setStatus(entity.getStatus());
		response.setTotalValue(entity.getTotalValue());
		response.setSale(saleAdapter.toJson(entity.getSale()));
		return response;
	}

	@Override
	public List<PaymentResponse> toJson(List<Payment> entities) {
		return entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
	}

}
