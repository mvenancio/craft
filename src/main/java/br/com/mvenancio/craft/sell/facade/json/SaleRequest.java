package br.com.mvenancio.craft.sell.facade.json;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.commons.joda.DatePatterns;
import br.com.mvenancio.craft.commons.joda.LocalDateDeserializer;
import br.com.mvenancio.craft.commons.joda.LocalDateSerializer;

public class SaleRequest extends AbstractJson {

	private Long id;
	private Long sellerId;
	private Long customerId;
	private BigDecimal totalValue;
	private BigDecimal discountValue;
	private BigDecimal discountPercentage;
	private List<ItemRequest> items;
	
	@JsonFormat(pattern = DatePatterns.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate deliveryDate;
	
	@JsonFormat(pattern = DatePatterns.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate firstDueDate;

	@JsonFormat(pattern = DatePatterns.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate partyDate;
	
	private Integer installments;
	private String observation;
	
	private BigDecimal workValue;

	private BigDecimal profitValue;

	private BigDecimal totalCostsValue;

	private BigDecimal rateValue;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSellerId() {
		return sellerId;
	}

	public void setSellerId(Long sellerId) {
		this.sellerId = sellerId;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public List<ItemRequest> getItems() {
		return items;
	}

	public void setItems(List<ItemRequest> items) {
		this.items = items;
	}

	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	public BigDecimal getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(BigDecimal discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public LocalDate getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDate deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public LocalDate getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(LocalDate firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}

	public LocalDate getPartyDate() {
		return partyDate;
	}

	public void setPartyDate(LocalDate partyDate) {
		this.partyDate = partyDate;
	}

	public BigDecimal getWorkValue() {
		return workValue;
	}

	public void setWorkValue(BigDecimal workValue) {
		this.workValue = workValue;
	}

	public BigDecimal getProfitValue() {
		return profitValue;
	}

	public void setProfitValue(BigDecimal profitValue) {
		this.profitValue = profitValue;
	}

	public BigDecimal getTotalCostsValue() {
		return totalCostsValue;
	}

	public void setTotalCostsValue(BigDecimal totalCostsValue) {
		this.totalCostsValue = totalCostsValue;
	}

	public BigDecimal getRateValue() {
		return rateValue;
	}

	public void setRateValue(BigDecimal rateValue) {
		this.rateValue = rateValue;
	}
	
}
