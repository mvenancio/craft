package br.com.mvenancio.craft.sell.facade.json;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.commons.joda.DatePatterns;
import br.com.mvenancio.craft.commons.joda.LocalDateDeserializer;
import br.com.mvenancio.craft.commons.joda.LocalDateSerializer;
import br.com.mvenancio.craft.sell.model.PaymentStatus;

public class PaymentResponse extends AbstractJson {

	private Long id;

	private PaymentStatus status;

	private SaleResponse sale;

	@JsonFormat(pattern = DatePatterns.DATE)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dueDate;

	@JsonFormat(pattern = DatePatterns.DATE)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate paymentDate;

	private BigDecimal totalValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PaymentStatus getStatus() {
		return status;
	}

	public void setStatus(PaymentStatus status) {
		this.status = status;
	}

	public SaleResponse getSale() {
		return sale;
	}

	public void setSale(SaleResponse sale) {
		this.sale = sale;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}

}
