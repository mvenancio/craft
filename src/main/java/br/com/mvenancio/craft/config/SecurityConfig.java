package br.com.mvenancio.craft.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.util.ResponseUtil;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String REALM_NAME = "handicraft";

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ResponseUtil<AbstractJson> responseUtil;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/healthcheck/**", "/notifications");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().fullyAuthenticated();
        http.httpBasic().realmName(REALM_NAME).authenticationEntryPoint(unauthorizedEntryPoint());
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint());
        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        // Basic Authentication with JSON Response
        return (HttpServletRequest req, HttpServletResponse res, AuthenticationException ex) -> {
            res.addHeader("WWW-Authenticate", "Basic realm=\"" + REALM_NAME + "\"");

            res.setContentType(MediaType.APPLICATION_JSON_VALUE);
            res.setStatus(HttpStatus.UNAUTHORIZED.value());
            ResponseEntity<AbstractJson> response = responseUtil.buildResponse(new AbstractJson(), HttpStatus.UNAUTHORIZED);
            om.writeValue(res.getOutputStream(), response.getBody());
        };
    }
}
