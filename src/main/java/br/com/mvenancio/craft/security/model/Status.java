package br.com.mvenancio.craft.security.model;

public enum Status {
	PENDING_CONFIRMATION, BLOCKED, AUTHORIZED;
}
