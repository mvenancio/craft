package br.com.mvenancio.craft.security.service.dto;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.signature.model.Signature;

public class UserSignatureWrapper {
	
	public UserSignatureWrapper(User user, Signature signature) {
		this.user = user;
		this.signature = signature;
	}

	private User user;
	private Signature signature;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Signature getSignature() {
		return signature;
	}

	public void setSignature(Signature signature) {
		this.signature = signature;
	}

}
