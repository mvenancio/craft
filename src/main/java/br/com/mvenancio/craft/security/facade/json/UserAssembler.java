package br.com.mvenancio.craft.security.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.model.UserDetail;
import br.com.mvenancio.craft.security.service.dto.UserSignatureWrapper;

@Component
public class UserAssembler implements Assembler<User, UserJson> {

	@Value("${pagseguro.redirect-page-signature}")
	private String baseUrl;
    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public UserJson toJson(User entity) {
        final UserJson json = new UserJson();
        json.setId(entity.getId());
        json.setEmail(entity.getEmail());
        json.setToken(entity.getToken());
        return json;
    }
    
    public UserJson toJson(UserSignatureWrapper wrapper) {
    	User entity = wrapper.getUser();
        final UserJson json = new UserJson();
        json.setId(entity.getId());
        json.setEmail(entity.getEmail());
        json.setToken(entity.getToken());
        json.setRedirectUrl(baseUrl+wrapper.getSignature().getRedirectCode());
        return json;
    }

    public UserDetailsJson toJson(UserDetail entity) {
        UserDetailsJson userDetails = new UserDetailsJson();
        userDetails.setId(entity.getId());
        userDetails.setAddress(entity.getAddress());
        userDetails.setBirth(entity.getBirth());
        userDetails.setCity(entity.getCity());
        userDetails.setComplement(entity.getComplement());
        userDetails.setDocument(entity.getDocument());
        userDetails.setDocument2(entity.getDocument2());
        userDetails.setMobilePhone(entity.getMobilePhone());
        userDetails.setName(entity.getName());
        userDetails.setNeighborhood(entity.getNeighborhood());
        userDetails.setNumber(entity.getNumber());
        userDetails.setObservation(entity.getObservation());
        userDetails.setPhone(entity.getPhone());
        userDetails.setPostalCode(entity.getPostalCode());
        userDetails.setState(entity.getState());
        userDetails.setUser(toJson(entity.getUser()));
        return userDetails;
    }

    public UserDetail toEntity(UserDetailsJson entity) {
        UserDetail userDetails = new UserDetail();
        userDetails.setId(entity.getId());
        userDetails.setAddress(entity.getAddress());
        userDetails.setBirth(entity.getBirth());
        userDetails.setCity(entity.getCity());
        userDetails.setComplement(entity.getComplement());
        userDetails.setDocument(entity.getDocument());
        userDetails.setDocument2(entity.getDocument2());
        userDetails.setMobilePhone(entity.getMobilePhone());
        userDetails.setName(entity.getName());
        userDetails.setNeighborhood(entity.getNeighborhood());
        userDetails.setNumber(entity.getNumber());
        userDetails.setObservation(entity.getObservation());
        userDetails.setPhone(entity.getPhone());
        userDetails.setPostalCode(entity.getPostalCode());
        userDetails.setState(entity.getState());
        return userDetails;
    }

    @Override
    public List<UserJson> toJson(List<User> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }

}
