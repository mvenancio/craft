package br.com.mvenancio.craft.security.repository;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class UserRepository extends JpaRepository<User, Long> {

	@Override
	protected Class<User> getEntityClass() {
		return User.class;
	}

	public User findByEmail(String email) {
		return this.queryOne(new JpaNamedQuery<>(getEntityClass(), User.FIND_BY_EMAIL, email));
	}

	public User findByToken(String token) {
		return this.queryOne(new JpaNamedQuery<>(getEntityClass(), User.FIND_BY_TOKEN, token));
	}

}
