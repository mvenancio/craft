package br.com.mvenancio.craft.security.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class UserAlreadyExistsException extends CraftException {

    private static final long serialVersionUID = 3925028974751893923L;

    public UserAlreadyExistsException() {
        super("User already exists", HttpStatus.UNPROCESSABLE_ENTITY);
    }

}
