package br.com.mvenancio.craft.security.facade;

import javax.mail.MessagingException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.commons.facade.MessageJson;
import br.com.mvenancio.craft.security.facade.json.UserAssembler;
import br.com.mvenancio.craft.security.facade.json.UserDetailsJson;
import br.com.mvenancio.craft.security.facade.json.UserJson;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/users")
public class UserFacade {

	@Autowired
	private ResponseUtil<UserJson> responseUtil;

	@Autowired
	private ResponseUtil<UserDetailsJson> responseDetailUtil;

	@Autowired
	private UserAssembler userAssembler;

	@Autowired
	private UserService userService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findAll() {
		return responseUtil.buildResponse(userAssembler.toJson(userService.findAll()));
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void forgotPassword(@RequestParam("email") String email) {
		userService.forgotPassword(email);
	}

	@RequestMapping(value="/email", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findByEmail(@RequestParam("email") String email) {
		return responseUtil.buildResponse(userAssembler.toJson(userService.findByEmail(email)));
	}
	
	@RequestMapping(value="/token", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findByToken(@RequestParam("token") String token) {
		return responseUtil.buildResponse(userAssembler.toJson(userService.findByToken(token)));
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> post(@Valid @RequestBody UserJson user) {
		return responseUtil.buildResponse(
				userAssembler.toJson(userService.add(user.getEmail(), user.getPassword(), user.getName())));
	}

	@RequestMapping(value = "/{id}/details", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> postDetails(@PathVariable("id") @NotNull Long userId,
			@Validated @RequestBody UserDetailsJson user) {
		return responseDetailUtil
				.buildResponse(userAssembler.toJson(userService.addDetail(userId, userAssembler.toEntity(user))));
	}

	@RequestMapping(value = "/{id}/details/{userDetailId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> putDetails(@PathVariable("userDetailId") @NotNull Long userDetailId,
			@Validated @RequestBody UserDetailsJson user) {
		return responseDetailUtil.buildResponse(
				userAssembler.toJson(userService.updateDetail(userDetailId, userAssembler.toEntity(user))));
	}

	@RequestMapping(value = "/{id}/email", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> putEmail(@PathVariable("id") @NotNull Long userId,
			@Validated @NotNull @Email @RequestBody String email) {
		return responseUtil.buildResponse(userAssembler.toJson(userService.updateEmail(userId, email)));
	}

	@RequestMapping(value = "/{id}/password", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> putPassword(@PathVariable("id") @NotNull Long userId,
			@Validated @NotNull @NotEmpty @Email @RequestBody UserJson userJson) {
		return responseUtil.buildResponse(userAssembler.toJson(userService.updatePassword(userId, userJson.getPassword())));
	}

	@RequestMapping(value = "/authorization", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> authorize(@RequestParam("token") @NotNull String token) {
		return responseUtil.buildResponse(userAssembler.toJson(userService.authorize(token)));
	}

	@RequestMapping(value = "/resend-authorization-mail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> resendAuthorizationMail(@RequestParam("email") @NotNull String email)
			throws MessagingException {
		userService.sendMail(email);
		return ResponseEntity.ok(new MessageJson("mail successfully sent"));
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> authenticate(@Validated @RequestBody UserJson user) {
		return responseUtil
				.buildResponse(userAssembler.toJson(userService.authenticate(user.getEmail(), user.getPassword())));
	}
}
