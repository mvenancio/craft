package br.com.mvenancio.craft.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.model.UserDetail;

@Repository
public interface UserDetailRepository extends JpaRepository<UserDetail, Long> {

	UserDetail findByUser(User user);

}
