package br.com.mvenancio.craft.security.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class UserNotFoundException extends CraftException {

	private static final long serialVersionUID = 3925028974751893923L;

	public UserNotFoundException(Long id) {
		super("User not found: id " + id, HttpStatus.NOT_FOUND);
	}

	public UserNotFoundException(String field, String value) {
		super("User not found: " + field + " " + value, HttpStatus.NOT_FOUND);
	}

}
