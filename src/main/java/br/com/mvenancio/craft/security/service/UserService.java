package br.com.mvenancio.craft.security.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import br.com.mvenancio.craft.security.model.Status;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.model.UserDetail;
import br.com.mvenancio.craft.security.repository.UserDetailRepository;
import br.com.mvenancio.craft.security.repository.UserRepository;
import br.com.mvenancio.craft.signature.service.SignatureService;
import br.com.mvenancio.craft.util.KeyGenerator;
import br.com.mvenancio.craft.util.MailUtil;

@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserRepository repository;

	@Autowired
	private UserDetailRepository userDetailRepository;

	@Autowired
	private KeyGenerator keyGenerator;

	@Autowired
	private SignatureService signatureService;

	@Autowired
	private MailUtil mailUtil;

	@Value("${user.authorization.base-url}")
	private String baseUrl;

	@Value("${user.forgot.base-url}")
	private String forgotPassword;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<User> findAll() {
		return repository.queryAll();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User add(String email, String password, String name) {
		User user = repository.findByEmail(email);
		if (user != null) {
			throw new UserAlreadyExistsException();
		}
		user = new User();
		user.setEmail(email);
		user.setPassword(encrypPassword(password));
		user.setToken(keyGenerator.generate());
		user.setCreationTime(LocalDateTime.now());
		user.setStatus(Status.AUTHORIZED);
		repository.add(user);
		UserDetail detail = new UserDetail();
		detail.setName(name);
		addDetail(user.getId(), detail);
		// String link = baseUrl + user.getToken();

		// Thread thread = new Thread() {
		// @Override
		// public void run() {
		// try {
		// mailUtil.sendEmail(email, link);
		// } catch (MessagingException e) {
		// e.printStackTrace();
		// }
		// }
		// };
		// thread.setPriority(Thread.MAX_PRIORITY);
		// thread.start();
		signatureService.add(user);
		return user;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public UserDetail addDetail(Long userId, UserDetail userDetail) {
		final User user = repository.get(userId);

		if (user == null) {
			throw new UserNotFoundException(userId);
		}
		userDetail.setUser(user);
		return userDetailRepository.save(userDetail);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public UserDetail updateDetail(Long userDetailId, UserDetail entity) {
		final UserDetail userDetail = userDetailRepository.findOne(userDetailId);
		if (userDetail == null) {
			throw new UserNotFoundException(userDetailId);
		}
		userDetail.setAddress(entity.getAddress());
		userDetail.setBirth(entity.getBirth());
		userDetail.setCity(entity.getCity());
		userDetail.setComplement(entity.getComplement());
		userDetail.setDocument(entity.getDocument());
		userDetail.setDocument2(entity.getDocument2());
		userDetail.setMobilePhone(entity.getMobilePhone());
		userDetail.setName(entity.getName());
		userDetail.setNeighborhood(entity.getNeighborhood());
		userDetail.setNumber(entity.getNumber());
		userDetail.setObservation(entity.getObservation());
		userDetail.setPhone(entity.getPhone());
		userDetail.setPostalCode(entity.getPostalCode());
		userDetail.setState(entity.getState());
		return userDetailRepository.save(userDetail);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User updateEmail(Long userId, String email) {
		final User user = repository.get(userId);

		if (user == null) {
			throw new UserNotFoundException(userId);
		}

		user.setEmail(email);
		return repository.update(user);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public User authenticate(String email, String password) {

		final User user = repository.findByEmail(email);
		if (user == null) {
			throw new UserNotFoundException("email", email);
		}

		final String encryptedPassword = encrypPassword(password);
		if (!encryptedPassword.equals(user.getPassword()) || Status.AUTHORIZED != user.getStatus()) {
			throw new UserNotAuthorizedException();
		}

		return user;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User updatePassword(Long userId, String password) {
		final User user = repository.get(userId);

		if (user == null) {
			throw new UserNotFoundException(userId);
		}

		user.setPassword(encrypPassword(password));
		return repository.update(user);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User authorize(String token) {
		final User user = repository.findByToken(token);

		if (user == null) {
			throw new UserNotFoundException("token", token);
		}
		user.setCreationTime(LocalDateTime.now());
		user.setStatus(Status.AUTHORIZED);
		return repository.update(user);
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public void sendMail(String email) throws MessagingException {
		final User user = repository.findByEmail(email);

		if (user == null) {
			throw new UserNotFoundException("email", email);
		}
		String link = baseUrl + user.getToken();
		mailUtil.sendEmail(email, link);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public User findByToken(String token) {
		final User user = repository.findByToken(token);
		if (user == null) {
			throw new UserNotFoundException("encodedId", token);
		}
		return user;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public void forgotPassword(String email) {
		final User user = repository.findByEmail(email);
		if (user == null) {
			throw new UserNotFoundException("email", email);
		}
		String link = forgotPassword + user.getToken();

		try {
			mailUtil.sendForgotEmail(email, link);
		} catch (MessagingException e) {
			LOGGER.error("[FORGOT-PASSWORD-ERROR] Error sending forgot password email");
		}

	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public User findByEmail(String email) {
		final User user = repository.findByEmail(email);
		if (user == null) {
			throw new UserNotFoundException("email", email);
		}
		return user;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public UserDetail findByUser(User user) {
		return userDetailRepository.findByUser(user);
	}

	String encrypPassword(String password) {
		final String initVector = "RandomInitVector";
		final String key = "Bar12345Bar12345";

		try {
			final IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			final SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			final byte[] encrypted = cipher.doFinal(password.getBytes());

			return Base64Utils.encodeToString(encrypted);
		} catch (final InvalidKeyException e) {
			e.printStackTrace();
		} catch (final UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (final NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (final NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (final InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (final IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (final BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
