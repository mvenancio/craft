package br.com.mvenancio.craft.security.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.security.model.UserDetail;
import br.com.mvenancio.craft.security.repository.UserDetailRepository;

@Service
public class UserDetailService {

    @Autowired
    private UserDetailRepository userRepository;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<UserDetail> findAll() {
        return userRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserDetail add(UserDetail userDetail) {
        return userRepository.save(userDetail);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public UserDetail update(Long id, UserDetail userDetail) {
        final UserDetail user = userRepository.findOne(id);

        if (user == null) {
            throw new UserNotFoundException(id);
        }

        user.setAddress(userDetail.getAddress());
        user.setBirth(userDetail.getBirth());
        user.setCity(userDetail.getCity());
        user.setComplement(userDetail.getComplement());
        user.setDocument(userDetail.getDocument());
        user.setDocument2(userDetail.getDocument2());
        user.setMobilePhone(userDetail.getMobilePhone());
        user.setName(userDetail.getName());
        user.setNeighborhood(userDetail.getNeighborhood());
        user.setNumber(userDetail.getNumber());
        user.setObservation(userDetail.getObservation());
        user.setPhone(userDetail.getPhone());
        user.setPostalCode(userDetail.getPostalCode());
        user.setState(userDetail.getState());

        return userRepository.save(user);
    }

}
