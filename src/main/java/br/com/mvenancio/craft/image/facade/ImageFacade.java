package br.com.mvenancio.craft.image.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.image.model.Image;
import br.com.mvenancio.craft.image.service.ImageService;

@RestController
@RequestMapping(value = "/craft-images", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageFacade {

	@Autowired
	private ImageService service;

	@RequestMapping(method = {RequestMethod.POST})
	public Image post(@RequestBody Image image) {
		return service.add(image);

	}
	@RequestMapping(method = {RequestMethod.PUT})
	public Image update(@RequestBody Image image) {
		return service.add(image);

	}

}
