package br.com.mvenancio.craft.image.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.image.model.Image;
import br.com.mvenancio.craft.image.repository.ImageRepository;

@Service
public class ImageService {

	@Autowired
	private ImageRepository repository;

	@Transactional(propagation = Propagation.REQUIRED)
	public Image add(Image image) {
		return repository.save(image);
	}

}
