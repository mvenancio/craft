package br.com.mvenancio.craft.image.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mvenancio.craft.image.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
