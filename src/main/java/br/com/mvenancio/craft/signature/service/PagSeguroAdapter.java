package br.com.mvenancio.craft.signature.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.model.UserDetail;
import br.com.mvenancio.craft.signature.dto.PreApproval;
import br.com.mvenancio.craft.signature.dto.PreApprovalRequest;
import br.com.mvenancio.craft.signature.dto.Sender;

@Component
public class PagSeguroAdapter {

	@Value("${pagseguro.success-redirect-page}")
	private String successRedirectPage;

	public PreApprovalRequest createRequest(User user, UserDetail detail) {
		String finalDate = LocalDateTime.now().plusYears(2).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		PreApprovalRequest request = new PreApprovalRequest();
		request.setRedirectURL(successRedirectPage);
		PreApproval preApproval = new PreApproval();
		preApproval.setCharge("auto");
		preApproval.setName("Assinatura Sr. Artesão");
		preApproval.setDetails("Assinatura mensal da plataforma de precificação e marketplace Sr. Artesão");
		preApproval.setAmountPerPayment(BigDecimal.TEN.setScale(2));
		preApproval.setPeriod("MONTHLY");
		preApproval.setFinalDate(finalDate);
		preApproval.setMaxTotalAmount(BigDecimal.TEN.multiply(BigDecimal.TEN).setScale(2));

		request.setPreApproval(preApproval);
		Sender sender = new Sender();
		sender.setName(detail.getName());
		sender.setEmail(user.getEmail());
		request.setSender(sender);
		return request;
	}

}
