package br.com.mvenancio.craft.signature.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name = "notification")
@Entity
public class SignatureNotification {

	private static final String ID_SEQ = "notification_id_seq";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	@Column(name = "notification_code")
	private String redirectCode;

	@Column(name = "notification_type")
	private String redirectType;

	private Integer tries;

	@Enumerated(EnumType.STRING)
	private SignatureNotificationStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRedirectCode() {
		return redirectCode;
	}

	public void setRedirectCode(String redirectCode) {
		this.redirectCode = redirectCode;
	}

	public String getRedirectType() {
		return redirectType;
	}

	public void setRedirectType(String redirectType) {
		this.redirectType = redirectType;
	}

	public Integer getTries() {
		return tries;
	}

	public void setTries(Integer tries) {
		this.tries = tries;
	}

	public SignatureNotificationStatus getStatus() {
		return status;
	}

	public void setStatus(SignatureNotificationStatus status) {
		this.status = status;
	}

}
