package br.com.mvenancio.craft.signature.dto;

public enum PaymentMethodType {

	CREDIT_CART(1), INVOICE(2), DEBIT(3), PAGSEGURO_BALANCE(4), OI(5), PAGSEGURO_DEPOSIT(7);

	private PaymentMethodType(int code) {
		this.code = code;
	}

	private int code;

	public int getCode() {
		return code;
	}
}
