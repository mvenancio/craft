package br.com.mvenancio.craft.signature.config;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.mvenancio.craft.signature.dto.BillingErrorWrapper;
import br.com.mvenancio.craft.signature.service.SignatureException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PagSeguroErrorDecoder implements ErrorDecoder {

	private static final Logger LOGGER = LoggerFactory.getLogger(PagSeguroErrorDecoder.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see feign.codec.ErrorDecoder#decode(java.lang.String, feign.Response)
	 */
	@Override
	public Exception decode(String methodKey, Response response) {
		LOGGER.info("Error on access " + methodKey + ". Response: " + response);
		BillingErrorWrapper error = parse(response.body().toString());
		throw new SignatureException("Error on " + methodKey, error);
	}

	BillingErrorWrapper parse(String xml) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(BillingErrorWrapper.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			StringReader reader = new StringReader(xml);
			return (BillingErrorWrapper) unmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			LOGGER.error("ERROR PARSIN");
		}
		return null;
	}

}
