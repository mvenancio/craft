package br.com.mvenancio.craft.signature.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;
import br.com.mvenancio.craft.signature.dto.BillingErrorWrapper;

public class SignatureException extends CraftException {
	
	private static final long serialVersionUID = -5832202402245385141L;
	
	private final BillingErrorWrapper error;

	public SignatureException(String message, BillingErrorWrapper error) {
		super(message, HttpStatus.BAD_REQUEST);
		this.error = error;
	}

	public BillingErrorWrapper getError() {
		return error;
	}

}
