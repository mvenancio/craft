package br.com.mvenancio.craft.signature.model;

public enum SignatureNotificationStatus {
	PENDING, CONFIRMED, DECLINED, EXPIRED;
}
