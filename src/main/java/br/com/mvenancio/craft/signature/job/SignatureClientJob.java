package br.com.mvenancio.craft.signature.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.mvenancio.craft.signature.service.SignatureService;

public class SignatureClientJob implements Job {
    @Autowired
	private SignatureService service;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        service.checkPendingSignatures();
    }
}