package br.com.mvenancio.craft.signature.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="checkout")
public class CheckoutResponse {

	private String code;

	private String date;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "PagSeguroCheckoutResponse [code=" + code + ", date=" + date + "]";
	}

}
