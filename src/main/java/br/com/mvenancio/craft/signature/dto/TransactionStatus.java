package br.com.mvenancio.craft.signature.dto;

public enum TransactionStatus {

	WAITING_PAYMENT(1), IN_ANALISYS(2), PAID(3), AVAILABLE(4), IN_CONFLICT(5), RETURNED(6), CANCELLED(
			7), CHARGE_BACK_RETURNED(8), IN_CONTEST(9);
	private TransactionStatus(int code) {
		this.code = code;
	}

	private int code;

	public int getCode() {
		return code;
	}
	
}
