package br.com.mvenancio.craft.signature.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "checkout")
public class CheckoutRequest {

	private String currency = "BRL";

	private String reference;

	private List<Item> items;

	public String getCurrency() {
		return currency;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	@XmlElementWrapper(name = "items")
	@XmlElement(name = "item")
	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
