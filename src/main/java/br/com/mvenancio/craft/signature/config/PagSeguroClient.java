package br.com.mvenancio.craft.signature.config;

import br.com.mvenancio.craft.signature.dto.CheckoutRequest;
import br.com.mvenancio.craft.signature.dto.CheckoutResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalCancelResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalQueryResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalRequest;
import br.com.mvenancio.craft.signature.dto.PreApprovalResponse;
import br.com.mvenancio.craft.signature.dto.TransactionDetailResponse;
import br.com.mvenancio.craft.signature.dto.TransactionSearchResponse;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

// https://pagseguro.uol.com.br/v2/guia-de-integracao/consulta-de-transacoes-por-intervalo-de-datas.html
public interface PagSeguroClient {

	@RequestLine("POST /v2/checkout?email={email}&token={token}")
	@Headers("Content-Type: application/xml; charset=ISO-8859-1")
	CheckoutResponse createPayent(@Param("email") String email, @Param("token") String token, CheckoutRequest request);

	@RequestLine("GET /v3/transactions?email={email}&token={token}&initialDate={initialDate}&finalDate={finalDate}")
	TransactionSearchResponse search(@Param("email") String email, @Param("token") String token,
			@Param("initialDate") String initialDate, @Param("finalDate") String finalDate);

	@RequestLine("GET /v3/transactions/{transactionCode}?email={email}&token={token}")
	TransactionDetailResponse detail(@Param("email") String email, @Param("token") String token,
			@Param("transactionCode") String transactionCode);
	
	@RequestLine("POST /v2/pre-approvals/request?email={email}&token={token}")
	@Headers("Content-Type: application/xml; charset=ISO-8859-1")
	PreApprovalResponse createSignature(@Param("email") String email, @Param("token") String token, PreApprovalRequest request);
	
	@RequestLine("GET /v2/pre-approvals/{transactionCode}?email={email}&token={token}")
	PreApprovalQueryResponse signatureDetail(@Param("email") String email, @Param("token") String token,
			@Param("transactionCode") String transactionCode);
	
	@RequestLine("GET /v2/pre-approvals/notifications/{transactionCode}?email={email}&token={token}")
	PreApprovalQueryResponse signatureDetailByNotification(@Param("email") String email, @Param("token") String token,
			@Param("transactionCode") String transactionCode);
	
	@RequestLine("GET /v2/pre-approvals/cancel/{transactionCode}?email={email}&token={token}")
	PreApprovalCancelResponse cancel(@Param("email") String email, @Param("token") String token,
			@Param("transactionCode") String transactionCode);

}
