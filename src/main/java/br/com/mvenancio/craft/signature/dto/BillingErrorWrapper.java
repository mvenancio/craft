package br.com.mvenancio.craft.signature.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "errors")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingErrorWrapper {

	@XmlElement(name = "error")
	private List<BillingError> errors;

	public List<BillingError> getErrors() {
		return errors;
	}

	public void setErrors(List<BillingError> errors) {
		this.errors = errors;
	}

}
