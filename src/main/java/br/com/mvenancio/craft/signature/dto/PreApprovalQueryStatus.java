package br.com.mvenancio.craft.signature.dto;

public enum PreApprovalQueryStatus {
	PENDING, ACTIVE, CANCELLED, CANCELLED_BY_RECEIVER, CANCELLED_BY_SENDER, EXPIRED;
}
