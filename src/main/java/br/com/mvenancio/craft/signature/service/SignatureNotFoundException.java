package br.com.mvenancio.craft.signature.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class SignatureNotFoundException extends CraftException {

	private static final long serialVersionUID = -8626261733824928761L;

	public SignatureNotFoundException(String message) {
		super(message, HttpStatus.NOT_FOUND);
	}

}
