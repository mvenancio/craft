package br.com.mvenancio.craft.signature.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.mvenancio.craft.commons.joda.DatePatterns;
import br.com.mvenancio.craft.security.model.User;

@Table(name = "signature")
@Entity
public class Signature {

	private static final String ID_SEQ = "signature_id_seq";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;
	@Column(name = "redirect_code")
	private String redirectCode;

	private String code;

	@Column(name = "creation_time")
	private LocalDateTime creationDate;

	@JsonFormat(pattern = DatePatterns.DATETIME)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@Column(name = "end_time")
	private LocalDateTime endDate;

	@Column(name = "first_due_date")
	private LocalDateTime firstDueDate;

	@Enumerated(EnumType.STRING)
	private SignatureStatus status;

	private String request;

	private String response;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SignatureStatus getStatus() {
		return status;
	}

	public void setStatus(SignatureStatus status) {
		this.status = status;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(LocalDateTime firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public String getRedirectCode() {
		return redirectCode;
	}

	public void setRedirectCode(String redirectCode) {
		this.redirectCode = redirectCode;
	}
}
