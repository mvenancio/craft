package br.com.mvenancio.craft.signature.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "paymentMethod")
public class PaymentMethod {

	private Integer type;
	private Integer code;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
