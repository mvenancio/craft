package br.com.mvenancio.craft.signature.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.signature.service.SignatureService;

@RestController
@RequestMapping("/notifications")
public class SignatureNotificationFacade {
	
	@Autowired
	private SignatureService service;

	@RequestMapping(method = RequestMethod.POST)
	public void create(@RequestParam("notificationCode") String notificationCode,
			@RequestParam("notificationType") String notificationType) {
		service.createNotification(notificationCode, notificationType);

	}

}
