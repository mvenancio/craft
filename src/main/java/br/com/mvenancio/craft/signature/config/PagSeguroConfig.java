package br.com.mvenancio.craft.signature.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Feign;
import feign.Feign.Builder;
import feign.Logger.Level;
import feign.jaxb.JAXBContextFactory;
import feign.jaxb.JAXBDecoder;
import feign.jaxb.JAXBEncoder;
import feign.slf4j.Slf4jLogger;

@Configuration
public class PagSeguroConfig {

	@Value("${pagseguro.base-url}")
	private String host;

	@Bean
	JAXBContextFactory jaxbFactory() {
		return new JAXBContextFactory.Builder().withMarshallerNoNamespaceSchemaLocation(host)
				.withMarshallerJAXBEncoding("ISO-8859-1").build();
	}

	@Bean
	JAXBEncoder encoder(JAXBContextFactory jaxbFactory) {
		return new JAXBEncoder(jaxbFactory);
	}

	@Bean
	JAXBDecoder decoder(JAXBContextFactory jaxbFactory) {
		return new JAXBDecoder(jaxbFactory);
	}

	@Bean
	public PagSeguroClient pagSeguroClient(JAXBEncoder encoder, JAXBDecoder decoder) {
		Builder builder = Feign.builder().encoder(encoder).logLevel(Level.FULL).decoder(decoder)
				.errorDecoder(new PagSeguroErrorDecoder()).logger(new Slf4jLogger());

		return builder.target(PagSeguroClient.class, host);
	}
}
