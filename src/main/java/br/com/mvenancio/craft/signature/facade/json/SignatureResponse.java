package br.com.mvenancio.craft.signature.facade.json;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.mvenancio.craft.commons.joda.DatePatterns;
import br.com.mvenancio.craft.signature.model.SignatureStatus;

public class SignatureResponse {

	private String redirectUrl;

	private String code;

	@JsonFormat(pattern = DatePatterns.DATETIME)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime creationDate;

	@JsonFormat(pattern = DatePatterns.DATETIME)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime endDate;

	@JsonFormat(pattern = DatePatterns.DATETIME)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime firstDueDate;

	private SignatureStatus status;

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public LocalDateTime getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(LocalDateTime firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public SignatureStatus getStatus() {
		return status;
	}

	public void setStatus(SignatureStatus status) {
		this.status = status;
	}
	
	
}
