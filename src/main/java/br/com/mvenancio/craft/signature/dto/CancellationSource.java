package br.com.mvenancio.craft.signature.dto;

public enum CancellationSource {

	INTERNAL("PagSeguro"), EXTERNAL("Instituições Financeiras");

	private CancellationSource(String description) {
		this.description = description;
	}

	private String description;

	public String getDescription() {
		return description;
	}

}
