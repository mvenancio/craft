package br.com.mvenancio.craft.signature.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "preApproval")
public class PreApprovalQueryResponse {

	private String name;
	private String code;
	private String date;
	private String tracker;
	private PreApprovalQueryStatus status;
	private String lastEventDate;
	private String charge;
	private Sender sender;

	public String getCode() {
		return code;
	}

	public String getDate() {
		return date;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTracker() {
		return tracker;
	}

	public void setTracker(String tracker) {
		this.tracker = tracker;
	}

	public PreApprovalQueryStatus getStatus() {
		return status;
	}

	public void setStatus(PreApprovalQueryStatus status) {
		this.status = status;
	}

	public String getLastEventDate() {
		return lastEventDate;
	}

	public void setLastEventDate(String lastEventDate) {
		this.lastEventDate = lastEventDate;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

}
