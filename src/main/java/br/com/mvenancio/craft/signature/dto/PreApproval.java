package br.com.mvenancio.craft.signature.dto;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="preApproval")
public class PreApproval {
	private String charge;
	private String name;
	private String details;
	private BigDecimal amountPerPayment;
	private String period;
	private String finalDate;
	private BigDecimal maxTotalAmount;

	public String getCharge() {
		return charge;
	}

	public String getName() {
		return name;
	}

	public String getDetails() {
		return details;
	}

	public BigDecimal getAmountPerPayment() {
		return amountPerPayment;
	}

	public String getPeriod() {
		return period;
	}

	public String getFinalDate() {
		return finalDate;
	}

	public BigDecimal getMaxTotalAmount() {
		return maxTotalAmount;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public void setAmountPerPayment(BigDecimal amountPerPayment) {
		this.amountPerPayment = amountPerPayment;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public void setFinalDate(String finalDate) {
		this.finalDate = finalDate;
	}

	public void setMaxTotalAmount(BigDecimal maxTotalAmount) {
		this.maxTotalAmount = maxTotalAmount;
	}
}
