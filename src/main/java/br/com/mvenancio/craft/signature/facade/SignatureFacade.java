package br.com.mvenancio.craft.signature.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.signature.facade.json.SignatureAdapter;
import br.com.mvenancio.craft.signature.facade.json.SignatureResponse;
import br.com.mvenancio.craft.signature.service.SignatureService;

@RestController
@RequestMapping("/signatures")
public class SignatureFacade {

	@Autowired
	private SignatureService service;

	@Autowired
	private SignatureAdapter adapter;

	@RequestMapping(method = RequestMethod.POST)
	public SignatureResponse create(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return adapter.toJson(service.createSignature(encodedUserId));
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public SignatureResponse inactive(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return adapter.toJson(service.inactive(encodedUserId));
	}

	@RequestMapping(method = RequestMethod.GET)
	public SignatureResponse get(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return adapter.toJson(service.findByUserToken(encodedUserId));
	}

}
