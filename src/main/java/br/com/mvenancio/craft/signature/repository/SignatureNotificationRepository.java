package br.com.mvenancio.craft.signature.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.signature.model.SignatureNotification;
import br.com.mvenancio.craft.signature.model.SignatureNotificationStatus;

@Repository
public interface SignatureNotificationRepository extends JpaRepository<SignatureNotification, Long> {
	List<SignatureNotification> findByStatus(SignatureNotificationStatus status);
}
