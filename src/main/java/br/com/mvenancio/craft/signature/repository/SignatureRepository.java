package br.com.mvenancio.craft.signature.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.signature.model.Signature;
import br.com.mvenancio.craft.signature.model.SignatureStatus;

@Repository
public interface SignatureRepository extends JpaRepository<Signature, Long> {

	Signature findByUser(User user);

	List<Signature> findByStatus(SignatureStatus status);

}
