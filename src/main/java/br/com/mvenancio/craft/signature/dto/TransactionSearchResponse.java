package br.com.mvenancio.craft.signature.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transactionSearchResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionSearchResponse {

	private String date;

	@XmlElementWrapper(name = "transactions")
	@XmlElement(name = "transaction")
	private List<TransactionResponse> transactions;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<TransactionResponse> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionResponse> transactions) {
		this.transactions = transactions;
	}

}
