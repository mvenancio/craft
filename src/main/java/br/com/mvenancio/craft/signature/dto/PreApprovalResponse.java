package br.com.mvenancio.craft.signature.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "preApprovalRequest")
public class PreApprovalResponse {

	private String code;
	private String date;

	public String getCode() {
		return code;
	}

	public String getDate() {
		return date;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
