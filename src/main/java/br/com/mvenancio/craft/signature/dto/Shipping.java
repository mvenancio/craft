package br.com.mvenancio.craft.signature.dto;

import java.math.BigDecimal;

public class Shipping {

	private Address address;
	private String type;
	private BigDecimal cost;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

}
