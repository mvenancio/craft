package br.com.mvenancio.craft.signature.model;

public enum SignatureStatus {
	PENDING, REQUESTED, REFUSED, ACTIVE, INACTIVE;
}
