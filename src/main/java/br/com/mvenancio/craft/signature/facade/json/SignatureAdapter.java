package br.com.mvenancio.craft.signature.facade.json;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.signature.model.Signature;

@Component
public class SignatureAdapter implements Assembler<Signature, SignatureResponse> {

	@Value("${pagseguro.redirect-page-signature}")
	private String baseUrl;

	@Override
	public SignatureResponse toJson(Signature entity) {
		SignatureResponse json = new SignatureResponse();
		json.setCode(entity.getCode());
		json.setCreationDate(entity.getCreationDate());
		json.setEndDate(entity.getEndDate());
		json.setFirstDueDate(entity.getFirstDueDate());
		json.setRedirectUrl(baseUrl + entity.getRedirectCode());
		json.setStatus(entity.getStatus());
		return json;
	}

	@Override
	public List<SignatureResponse> toJson(List<Signature> entities) {
		// TODO Auto-generated method stub
		return null;
	}

}
