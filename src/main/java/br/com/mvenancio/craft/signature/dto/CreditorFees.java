package br.com.mvenancio.craft.signature.dto;

import java.math.BigDecimal;

public class CreditorFees {

	private BigDecimal installmentFeeAmount;
	private BigDecimal intermediationRateAmount;
	private BigDecimal intermediationFeeAmount;

	public BigDecimal getInstallmentFeeAmount() {
		return installmentFeeAmount;
	}

	public void setInstallmentFeeAmount(BigDecimal installmentFeeAmount) {
		this.installmentFeeAmount = installmentFeeAmount;
	}

	public BigDecimal getIntermediationRateAmount() {
		return intermediationRateAmount;
	}

	public void setIntermediationRateAmount(BigDecimal intermediationRateAmount) {
		this.intermediationRateAmount = intermediationRateAmount;
	}

	public BigDecimal getIntermediationFeeAmount() {
		return intermediationFeeAmount;
	}

	public void setIntermediationFeeAmount(BigDecimal intermediationFeeAmount) {
		this.intermediationFeeAmount = intermediationFeeAmount;
	}
}
