package br.com.mvenancio.craft.signature.service;

import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.model.UserDetail;
import br.com.mvenancio.craft.security.service.UserService;
import br.com.mvenancio.craft.signature.config.PagSeguroClient;
import br.com.mvenancio.craft.signature.dto.PreApprovalQueryResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalRequest;
import br.com.mvenancio.craft.signature.dto.PreApprovalResponse;
import br.com.mvenancio.craft.signature.model.Signature;
import br.com.mvenancio.craft.signature.model.SignatureNotification;
import br.com.mvenancio.craft.signature.model.SignatureNotificationStatus;
import br.com.mvenancio.craft.signature.model.SignatureStatus;
import br.com.mvenancio.craft.signature.repository.SignatureNotificationRepository;
import br.com.mvenancio.craft.signature.repository.SignatureRepository;

@Service
public class SignatureService {
	private static final Logger LOGGER = LoggerFactory.getLogger(SignatureService.class);
	@Autowired
	private PagSeguroClient client;

	@Autowired
	private PagSeguroAdapter adapter;

	@Autowired
	private UserService userService;

	@Value("${pagseguro.email}")
	private String email;

	@Value("${pagseguro.token}")
	private String token;

	@Autowired
	private SignatureRepository repository;
	@Autowired
	private SignatureNotificationRepository notificationRepository;

	@Transactional(propagation = Propagation.REQUIRED)
	public Signature add(User user) {
		long months = 1L;
		if (user.getId() <= 150) {
			months = 12;
		}
		Signature signature = repository.findByUser(user);
		if (signature == null) {
			signature = new Signature();
			signature.setUser(user);
			signature.setStatus(SignatureStatus.ACTIVE);
		}
		if (signature.getEndDate() == null) {
			signature.setEndDate(LocalDateTime.now().plusMonths(months));
		}
		return repository.save(signature);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Signature createSignature(User user, UserDetail detail) {
		Signature signature = add(user);
		if (SignatureStatus.REQUESTED == signature.getStatus() || SignatureStatus.ACTIVE == signature.getStatus()) {
			return signature;
		}
		PreApprovalRequest request = adapter.createRequest(user, detail);
		PreApprovalResponse response = client.createSignature(email, token, request);
		signature.setRedirectCode(response.getCode());
		signature.setStatus(SignatureStatus.REQUESTED);
		signature.setRequest(asString(request));
		signature.setResponse(asString(response));
		return repository.save(signature);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Signature createSignature(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		UserDetail detail = userService.findByUser(user);
		return createSignature(user, detail);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Signature findByUserToken(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Signature signature = repository.findByUser(user);
		if (signature == null) {
			throw new SignatureNotFoundException("Signature for this user was not found");
		}
		return signature;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Signature inactive(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		
		Signature signature = repository.findByUser(user);
		if (signature == null) {
			throw new SignatureNotFoundException("Signature for this user was not found");
		}
		try {
			client.cancel(email, token, signature.getCode());
		} catch (SignatureException e) {
			LOGGER.info("Erro ao cancelar a assinatura no pag seguro " + e.getMessage());
		}
		signature.setStatus(SignatureStatus.INACTIVE);
		return repository.save(signature);
	}

	public String asString(Object object) {
		try {
			JAXBContext pContext = JAXBContext.newInstance(object.getClass());
			java.io.StringWriter sw = new StringWriter();
			Marshaller marshaller = pContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			marshaller.marshal(object, sw);
			return sw.toString();
		} catch (Exception e) {
			return null;
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public void checkPendingSignatures() {
		try {
			List<SignatureNotification> notifications = notificationRepository.findByStatus(SignatureNotificationStatus.PENDING);
			notifications.forEach(notification -> {
				PreApprovalQueryResponse response = client.signatureDetail(email, token, notification.getRedirectCode());
				User user = userService.findByEmail(response.getSender().getEmail());
				Signature signature = repository.findByUser(user);
				
				switch (response.getStatus()) {
				case ACTIVE:
					signature.setStatus(SignatureStatus.ACTIVE);
					break;
				case CANCELLED:
					signature.setStatus(SignatureStatus.REFUSED);
					break;
				case CANCELLED_BY_RECEIVER:
					signature.setStatus(SignatureStatus.INACTIVE);
					break;
				case CANCELLED_BY_SENDER:
					signature.setStatus(SignatureStatus.INACTIVE);
					break;
				default:
					break;
				}
				
				
			});
		} catch (SignatureException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public SignatureNotification createNotification(String code, String type) {
		SignatureNotification notification = new SignatureNotification();
		notification.setRedirectCode(code);
		notification.setRedirectType(type);
		notification.setStatus(SignatureNotificationStatus.PENDING);
		notification.setTries(0);
		return notificationRepository.save(notification);
	}

}
