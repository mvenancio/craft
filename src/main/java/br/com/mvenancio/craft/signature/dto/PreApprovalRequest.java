package br.com.mvenancio.craft.signature.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "preApprovalRequest")
public class PreApprovalRequest {

	private String redirectURL;
	
	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	private Sender sender;
	private PreApproval preApproval;

	public Sender getSender() {
		return sender;
	}

	public PreApproval getPreApproval() {
		return preApproval;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public void setPreApproval(PreApproval preApproval) {
		this.preApproval = preApproval;
	}

}
