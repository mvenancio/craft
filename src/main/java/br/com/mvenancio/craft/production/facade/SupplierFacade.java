package br.com.mvenancio.craft.production.facade;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.production.facade.json.SupplierAdapter;
import br.com.mvenancio.craft.production.facade.json.SupplierDTO;
import br.com.mvenancio.craft.production.service.SupplierService;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/suppliers")
public class SupplierFacade {

	@Autowired
	private SupplierService service;

	@Autowired
	private ResponseUtil<SupplierDTO> responseUtil;

	@Autowired
	private SupplierAdapter adapter;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findAll(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toDto(service.findAll(encodedUserId)));
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> post(@Validated @RequestBody SupplierDTO request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toDto(service.add(adapter.toEntity(request), encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> put(@PathVariable("id") @NotNull Long id,
			@Validated @RequestBody SupplierDTO request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(adapter.toDto(service.update(id, adapter.toEntity(request), encodedUserId)));
	}
}
