package br.com.mvenancio.craft.production.repository;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.production.model.ProductDepreciation;

@Repository
public class ProductDepreciationRepository extends JpaRepository<ProductDepreciation, Long> {

    @Override
    protected Class<ProductDepreciation> getEntityClass() {
        return ProductDepreciation.class;
    }

}
