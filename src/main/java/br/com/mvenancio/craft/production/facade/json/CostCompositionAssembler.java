package br.com.mvenancio.craft.production.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.cost.facade.json.DepreciationCostAssembler;
import br.com.mvenancio.craft.production.model.ProductDepreciation;

@Component
public class CostCompositionAssembler implements Assembler<ProductDepreciation, CostCompositionJson> {

	@Autowired
	private DepreciationCostAssembler assembler;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
	 */
	@Override
	public CostCompositionJson toJson(ProductDepreciation entity) {
		CostCompositionJson json = new CostCompositionJson();
		json.setValue(entity.getValue());
		json.setDepreciation(assembler.toJson(entity.getDepreciationCost()));
		return json;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
	 */
	@Override
	public List<CostCompositionJson> toJson(List<ProductDepreciation> entities) {
		return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
	}

}
