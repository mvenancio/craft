package br.com.mvenancio.craft.production.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.production.model.Stuff;

@Component
public class StuffAssembler implements Assembler<Stuff, StuffJson> {

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public StuffJson toJson(Stuff entity) {
        StuffJson json = new StuffJson();
        json.setId(entity.getId());
        json.setDescription(entity.getDescription());
        json.setStock(entity.getStock());
        json.setUnitPrice(entity.getUnitPrice());
        return json;
    }

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<StuffJson> toJson(List<Stuff> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }
}
