package br.com.mvenancio.craft.production.facade.json;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class StuffJson extends AbstractJson {

    private Long id;

    @NotEmpty
    @NotNull
    @Size(min=1, max=255)
    private String description;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "9999999999.99")
    private BigDecimal unitPrice;

    @DecimalMin(value = "0.000001")
    @DecimalMax(value = "9999999999.99")
    private BigDecimal stock;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the stock
     */
    public BigDecimal getStock() {
        return stock;
    }

    /**
     * @param stock
     *            the stock to set
     */
    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice
     *            the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

}
