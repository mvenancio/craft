package br.com.mvenancio.craft.production.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Table(name = "product")
@Entity
@NamedQuery(name = Product.FIND_BY_USER, query = "SELECT p FROM Product p WHERE p.user = ?1 ORDER BY p.id")
public class Product {

	private static final String ID_SEQ = "product_id_seq";
	public static final String FIND_BY_USER = "Product-FindByUser";

	@Id
	@SequenceGenerator(name = ID_SEQ, sequenceName = ID_SEQ, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
	private Long id;

	private String description;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Composition> items;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProductDepreciation> depreciationCosts;
	
	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ProductImage> images;

	private BigDecimal price;

	private BigDecimal percentage;

	@Column(name = "fixed_cost")
	private BigDecimal fixedCost;

	@Column(name = "composition_cost")
	private BigDecimal compositionCost;
	
	@Column(name = "depreciation_cost")
	private BigDecimal depreciationCost;

	@Column(name = "work_cost")
	private BigDecimal workCost;

	@Column(name = "total_cost")
	private BigDecimal totalCost;

	private Long productionTime;

	@OneToOne
	@JoinColumn(name = "craft_user_id")
	private User user;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the items
	 */
	public List<Composition> getItems() {
		return items;
	}

	/**
	 * @param items
	 *            the items to set
	 */
	public void setItems(List<Composition> items) {
		this.items = items;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the percentage
	 */
	public BigDecimal getPercentage() {
		return percentage;
	}

	/**
	 * @param percentage
	 *            the percentage to set
	 */
	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	/**
	 * @return the productionTime
	 */
	public Long getProductionTime() {
		return productionTime;
	}

	/**
	 * @param productionTime
	 *            the productionTime to set
	 */
	public void setProductionTime(Long productionTime) {
		this.productionTime = productionTime;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public List<ProductDepreciation> getDepreciationCosts() {
		return depreciationCosts;
	}

	public void setDepreciationCosts(List<ProductDepreciation> depreciationCosts) {
		this.depreciationCosts = depreciationCosts;
	}

	public BigDecimal getFixedCost() {
		return fixedCost;
	}

	public void setFixedCost(BigDecimal fixedCost) {
		this.fixedCost = fixedCost;
	}

	public BigDecimal getDepreciationCost() {
		return depreciationCost;
	}

	public void setDepreciationCost(BigDecimal depreciationCost) {
		this.depreciationCost = depreciationCost;
	}

	public BigDecimal getWorkCost() {
		return workCost;
	}

	public void setWorkCost(BigDecimal workCost) {
		this.workCost = workCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getCompositionCost() {
		return compositionCost;
	}

	public void setCompositionCost(BigDecimal compositionCost) {
		this.compositionCost = compositionCost;
	}

	public List<ProductImage> getImages() {
		return images;
	}

	public void setImages(List<ProductImage> images) {
		this.images = images;
	}
	
	

}
