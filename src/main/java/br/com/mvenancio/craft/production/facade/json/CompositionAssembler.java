package br.com.mvenancio.craft.production.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.production.model.Composition;

@Component
public class CompositionAssembler  implements Assembler<Composition, CompositionJson> {

    @Autowired
    private StuffAssembler stuffAssembler;
    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public CompositionJson toJson(Composition entity) {
        CompositionJson json = new CompositionJson();
        json.setQuantity(entity.getQuantity());
        json.setStuff(stuffAssembler.toJson(entity.getStuff()));
        return json;
    }

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<CompositionJson> toJson(List<Composition> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }


}
