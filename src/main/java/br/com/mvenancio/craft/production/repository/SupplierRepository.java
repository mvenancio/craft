	package br.com.mvenancio.craft.production.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.production.model.Supplier;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class SupplierRepository extends JpaRepository<Supplier, Long> {

	@Override
	protected Class<Supplier> getEntityClass() {
		return Supplier.class;
	}

	public List<Supplier> findByUser(User user) {
		return this.query(new JpaNamedQuery<>(getEntityClass(), Supplier.FIND_BY_USER, user));
	}

}
