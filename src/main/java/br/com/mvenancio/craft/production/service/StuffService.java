package br.com.mvenancio.craft.production.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.production.model.Stuff;
import br.com.mvenancio.craft.production.repository.StuffRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class StuffService {

    @Autowired
    private StuffRepository repository;

    @Autowired
    private UserService userService;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<Stuff> findAll(String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        return repository.findByUser(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Stuff add(String description, BigDecimal unitPrice, BigDecimal stock, String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        Stuff stuff = new Stuff();
        stuff.setDescription(description);
        stuff.setStock(stock);
        stuff.setUnitPrice(unitPrice);
        stuff.setUser(user);
        return repository.add(stuff);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Stuff update(Long id, String description, BigDecimal unitPrice, BigDecimal stock, String encodedUserId) {
        User user = userService.findByToken(encodedUserId);

        Stuff stuff = repository.get(id);

        if (stuff == null || !stuff.getUser().equals(user)) {
            throw new StuffNotFoundException(id);
        }
        stuff.setDescription(description);
        stuff.setStock(stock);
        stuff.setUnitPrice(unitPrice);
        return repository.update(stuff);
    }

    public Stuff get(Long id, String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        Stuff stuff = repository.get(id);

        if (stuff == null || !stuff.getUser().equals(user)) {
            throw new StuffNotFoundException(id);
        }
        return stuff;
    }

}
