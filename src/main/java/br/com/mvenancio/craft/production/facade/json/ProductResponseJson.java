package br.com.mvenancio.craft.production.facade.json;

import java.math.BigDecimal;
import java.util.List;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.image.model.Image;

public class ProductResponseJson extends AbstractJson {

    private Long id;

    private String description;

    private BigDecimal price;
    
    private BigDecimal percentage;
    
    private BigDecimal fixedCost;
	
	private BigDecimal depreciationCost;
	
	private BigDecimal workCost;
	
	private BigDecimal totalCost;
	
	private BigDecimal compositionCost;

    private Long productionTime;

    private List<CompositionJson> items;
    
    private List<CostCompositionJson> costs;
    
    private List<Image> images;
    
    private Long principalImageId;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the productionTime
     */
    public Long getProductionTime() {
        return productionTime;
    }

    /**
     * @param productionTime the productionTime to set
     */
    public void setProductionTime(Long productionTime) {
        this.productionTime = productionTime;
    }

    /**
     * @return the items
     */
    public List<CompositionJson> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<CompositionJson> items) {
        this.items = items;
    }

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public List<CostCompositionJson> getCosts() {
		return costs;
	}

	public void setCosts(List<CostCompositionJson> costs) {
		this.costs= costs;
	}

	public BigDecimal getFixedCost() {
		return fixedCost;
	}

	public void setFixedCost(BigDecimal fixedCost) {
		this.fixedCost = fixedCost;
	}

	public BigDecimal getDepreciationCost() {
		return depreciationCost;
	}

	public void setDepreciationCost(BigDecimal depreciationCost) {
		this.depreciationCost = depreciationCost;
	}

	public BigDecimal getWorkCost() {
		return workCost;
	}

	public void setWorkCost(BigDecimal workCost) {
		this.workCost = workCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getCompositionCost() {
		return compositionCost;
	}

	public void setCompositionCost(BigDecimal compositionCost) {
		this.compositionCost = compositionCost;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public Long getPrincipalImageId() {
		return principalImageId;
	}

	public void setPrincipalImageId(Long principalImageId) {
		this.principalImageId = principalImageId;
	}

}
