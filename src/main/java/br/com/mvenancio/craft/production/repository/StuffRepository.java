package br.com.mvenancio.craft.production.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.production.model.Stuff;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class StuffRepository extends JpaRepository<Stuff, Long> {

    @Override
    protected Class<Stuff> getEntityClass() {
        return Stuff.class;
    }

    public List<Stuff> findByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Stuff.FIND_BY_USER, user));
    }

}
