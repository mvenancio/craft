package br.com.mvenancio.craft.production.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class StuffNotFoundException extends CraftException {

    private static final long serialVersionUID = -6663796093080480165L;

    public StuffNotFoundException(Long id) {
        super("Stuff not found: id " + id, HttpStatus.NOT_FOUND);
    }

}
