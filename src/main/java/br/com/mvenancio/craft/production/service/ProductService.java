package br.com.mvenancio.craft.production.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Objects;

import br.com.mvenancio.craft.cost.service.DepreciationCostService;
import br.com.mvenancio.craft.image.repository.ImageRepository;
import br.com.mvenancio.craft.production.facade.json.ProductRequestJson;
import br.com.mvenancio.craft.production.model.Composition;
import br.com.mvenancio.craft.production.model.Product;
import br.com.mvenancio.craft.production.model.ProductDepreciation;
import br.com.mvenancio.craft.production.model.ProductImage;
import br.com.mvenancio.craft.production.repository.ProductRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private StuffService stuffService;

	@Autowired
	private DepreciationCostService depreciationCostService;

	@Autowired
	private UserService userService;

	@Autowired
	private ImageRepository imageRepository;

	@Transactional(propagation = Propagation.REQUIRED)
	public Product add(ProductRequestJson request, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);

		Product product = new Product();
		fillProduct(request, encodedUserId, product);
		product.setUser(user);
		return repository.add(product);
	}

	private void fillProduct(ProductRequestJson request, String encodedUserId, Product product) {
		product.setDescription(request.getDescription());
		product.setPrice(request.getPrice());
		product.setProductionTime(request.getProductionTime());
		product.setPercentage(request.getPercentage());
		product.setFixedCost(request.getFixedCost());
		product.setDepreciationCost(request.getDepreciationCost());
		product.setWorkCost(request.getWorkCost());
		product.setTotalCost(request.getTotalCost());
		product.setCompositionCost(request.getCompositionCost());
		fillItems(request, encodedUserId, product);
		fillDepreciationCosts(request, encodedUserId, product);
		fillImages(request, product);
	}

	private void fillImages(ProductRequestJson request, Product product) {
		product.setImages(Objects.firstNonNull(product.getImages(), new ArrayList<>()));
		product.getImages().clear();
		List<ProductImage> images = Objects.firstNonNull(request.getImageIds(), new ArrayList<Long>()).stream()
				.map(id -> new ProductImage(product, imageRepository.findOne(id))).collect(Collectors.toList());
		product.getImages().addAll(images);
	}

	private void fillDepreciationCosts(ProductRequestJson request, String encodedUserId, Product product) {
		product.setDepreciationCosts(Objects.firstNonNull(product.getDepreciationCosts(), new ArrayList<>()));
		product.getDepreciationCosts().clear();

		List<ProductDepreciation> costs = Objects
				.firstNonNull(request.getCostComposition().entrySet(), new ArrayList<Entry<Long, BigDecimal>>())
				.stream().map((entry) -> {
					ProductDepreciation productDepreciation = new ProductDepreciation();
					productDepreciation.setProduct(product);
					productDepreciation.setDepreciationCost(depreciationCostService.get(entry.getKey(), encodedUserId));
					productDepreciation.setValue(entry.getValue());
					return productDepreciation;
				}).collect(Collectors.toList());

		product.getDepreciationCosts().addAll(costs);
	}

	private void fillItems(ProductRequestJson request, String encodedUserId, Product product) {
		product.setItems(Objects.firstNonNull(product.getItems(), new ArrayList<>()));
		product.getItems().clear();
		List<Composition> items = request.getComposition() == null ? new ArrayList<>()
				: request.getComposition().entrySet().stream().map((entry) -> {
					Composition composition = new Composition();
					composition.setProduct(product);
					composition.setStuff(stuffService.get(entry.getKey(), encodedUserId));
					composition.setQuantity(entry.getValue());
					return composition;
				}).collect(Collectors.toList());
		product.getItems().addAll(items);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Product> findAll(String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		return repository.findByUser(user);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Product update(Long id, ProductRequestJson request, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Product product = repository.get(id);
		if (product == null || !product.getUser().equals(user)) {
			throw new ProductNotFoundException(id);
		}
		fillProduct(request, encodedUserId, product);
		return repository.update(product);

	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Product get(Long id, String encodedUserId) {
		User user = userService.findByToken(encodedUserId);
		Product product = repository.get(id);
		if (product == null || !product.getUser().equals(user)) {
			throw new ProductNotFoundException(id);
		}
		return product;
	}

}
