package br.com.mvenancio.craft.production.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class SupplierNotFoundException extends CraftException {

	private static final long serialVersionUID = 8627281241509628084L;

	public SupplierNotFoundException(Long id) {
		super("Supplier not found: id " + id, HttpStatus.NOT_FOUND);
	}

}
