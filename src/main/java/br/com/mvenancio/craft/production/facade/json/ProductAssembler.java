package br.com.mvenancio.craft.production.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.production.model.Product;

@Component
public class ProductAssembler implements Assembler<Product, ProductResponseJson> {

    @Autowired
    private CompositionAssembler compositionAssembler;
    
    @Autowired
    private CostCompositionAssembler costCompositionAssembler;

    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public ProductResponseJson toJson(Product entity) {
        ProductResponseJson json = new ProductResponseJson();
        json.setDescription(entity.getDescription());
        json.setId(entity.getId());
        json.setPrice(entity.getPrice());
        json.setPercentage(entity.getPercentage());
        json.setProductionTime(entity.getProductionTime());
        json.setFixedCost(entity.getFixedCost());
        json.setDepreciationCost(entity.getDepreciationCost());
        json.setWorkCost(entity.getWorkCost());
        json.setCompositionCost(entity.getCompositionCost());
        json.setTotalCost(entity.getTotalCost());
        json.setItems(compositionAssembler.toJson(entity.getItems()));
        json.setCosts(costCompositionAssembler.toJson(entity.getDepreciationCosts()));
        return json;
    }


    /* (non-Javadoc)
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<ProductResponseJson> toJson(List<Product> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }


}
