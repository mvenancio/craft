package br.com.mvenancio.craft.production.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.production.model.Supplier;

@Component
public class SupplierAdapter {

	public List<SupplierDTO> toDto(List<Supplier> suppliers) {
		return suppliers.stream().map((supplier) -> toDto(supplier)).collect(Collectors.toList());
	}

	public SupplierDTO toDto(Supplier supplier) {
		SupplierDTO dto = new SupplierDTO();
		dto.setAddress(supplier.getAddress());
		dto.setCity(supplier.getCity());
		dto.setComplement(supplier.getComplement());
		dto.setDocument(supplier.getDocument());
		dto.setDocument2(supplier.getDocument2());
		dto.setEmail(supplier.getEmail());
		dto.setFacebook(supplier.getFacebook());
		dto.setGooglePlus(supplier.getGooglePlus());
		dto.setId(supplier.getId());
		dto.setMobilePhone(supplier.getMobilePhone());
		dto.setName(supplier.getName());
		dto.setNeighborhood(supplier.getNeighborhood());
		dto.setNumber(supplier.getNumber());
		dto.setObservation(supplier.getObservation());
		dto.setOther(supplier.getOther());
		dto.setPhone(supplier.getPhone());
		dto.setPostalCode(supplier.getPostalCode());
		dto.setState(supplier.getState());
		dto.setTwitter(supplier.getTwitter());
		return dto;
	}

	public Supplier toEntity(SupplierDTO dto) {
		Supplier supplier = new Supplier();
		supplier.setAddress(dto.getAddress());
		supplier.setCity(dto.getCity());
		supplier.setComplement(dto.getComplement());
		supplier.setDocument(dto.getDocument());
		supplier.setDocument2(dto.getDocument2());
		supplier.setEmail(dto.getEmail());
		supplier.setFacebook(dto.getFacebook());
		supplier.setGooglePlus(dto.getGooglePlus());
		supplier.setId(dto.getId());
		supplier.setMobilePhone(dto.getMobilePhone());
		supplier.setName(dto.getName());
		supplier.setNeighborhood(dto.getNeighborhood());
		supplier.setNumber(dto.getNumber());
		supplier.setObservation(dto.getObservation());
		supplier.setOther(dto.getOther());
		supplier.setPhone(dto.getPhone());
		supplier.setPostalCode(dto.getPostalCode());
		supplier.setState(dto.getState());
		supplier.setTwitter(dto.getTwitter());
		return supplier;
	}
}
