package br.com.mvenancio.craft.production.facade.json;

import java.math.BigDecimal;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.cost.facade.json.DepreciationCostJson;

public class CostCompositionJson extends AbstractJson {

	private DepreciationCostJson depreciation;

	private BigDecimal value;

	public DepreciationCostJson getDepreciation() {
		return depreciation;
	}

	public void setDepreciation(DepreciationCostJson depreciation) {
		this.depreciation = depreciation;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
