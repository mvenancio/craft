package br.com.mvenancio.craft.production.facade;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.production.facade.json.ProductAssembler;
import br.com.mvenancio.craft.production.facade.json.ProductRequestJson;
import br.com.mvenancio.craft.production.facade.json.ProductResponseJson;
import br.com.mvenancio.craft.production.service.ProductService;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/products")
public class ProductFacade {

	@Autowired
	private ResponseUtil<ProductResponseJson> responseUtil;

	@Autowired
	private ProductAssembler assembler;

	@Autowired
	private ProductService service;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findAll(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.findAll(encodedUserId)));
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> post(@Validated @RequestBody ProductRequestJson request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.add(request, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public ResponseEntity<AbstractJson> put(@PathVariable("id") @NotNull Long id,
			@Validated @RequestBody ProductRequestJson request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.update(id, request, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> get(@PathVariable("id") @NotNull Long userId,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.get(userId, encodedUserId)));
	}

}
