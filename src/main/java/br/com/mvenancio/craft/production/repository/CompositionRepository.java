package br.com.mvenancio.craft.production.repository;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.production.model.Composition;

@Repository
public class CompositionRepository extends JpaRepository<Composition, Long> {

    @Override
    protected Class<Composition> getEntityClass() {
        return Composition.class;
    }

}
