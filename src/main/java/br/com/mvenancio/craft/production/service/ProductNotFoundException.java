package br.com.mvenancio.craft.production.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class ProductNotFoundException extends CraftException{

    private static final long serialVersionUID = -5553949649861001481L;

    public ProductNotFoundException(Long id) {
        super("Product not found: "+id, HttpStatus.NOT_FOUND);
    }


}
