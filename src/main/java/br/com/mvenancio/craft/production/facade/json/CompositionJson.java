package br.com.mvenancio.craft.production.facade.json;

import java.math.BigDecimal;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class CompositionJson extends AbstractJson {

    private StuffJson stuff;

    private BigDecimal quantity;

    /**
     * @return the stuffJson
     */
    public StuffJson getStuff() {
        return stuff;
    }

    /**
     * @param stuffJson the stuffJson to set
     */
    public void setStuff(StuffJson stuffJson) {
        this.stuff = stuffJson;
    }

    /**
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }


}
