package br.com.mvenancio.craft.production.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.production.model.Supplier;
import br.com.mvenancio.craft.production.repository.SupplierRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class SupplierService {

	@Autowired
	private SupplierRepository repository;

	@Autowired
	private UserService userService;

	@Transactional(propagation=Propagation.REQUIRED)
	public Supplier add(Supplier supplier, String encodedId) {
		User user = userService.findByToken(encodedId);
		supplier.setUser(user);
		return repository.add(supplier);
	}
	
	
	@Transactional(propagation=Propagation.REQUIRED)
	public Supplier update(Long id, Supplier supplier, String encodedId) {
		User user = userService.findByToken(encodedId);
		Supplier persistedSupplier = repository.get(id);
        if (persistedSupplier == null || !persistedSupplier.getUser().equals(user)) {
            throw new SupplierNotFoundException(id);
        }
        persistedSupplier.setAddress(supplier.getAddress());
        persistedSupplier.setCity(supplier.getCity());
        persistedSupplier.setComplement(supplier.getComplement());
        persistedSupplier.setDocument(supplier.getDocument());
        persistedSupplier.setDocument2(supplier.getDocument2());
        persistedSupplier.setEmail(supplier.getEmail());
        persistedSupplier.setFacebook(supplier.getFacebook());
        persistedSupplier.setGooglePlus(supplier.getGooglePlus());
        persistedSupplier.setMobilePhone(supplier.getMobilePhone());
        persistedSupplier.setName(supplier.getName());
        persistedSupplier.setNeighborhood(supplier.getNeighborhood());
        persistedSupplier.setNumber(supplier.getNumber());
        persistedSupplier.setObservation(supplier.getObservation());
        persistedSupplier.setOther(supplier.getOther());
        persistedSupplier.setPhone(supplier.getPhone());
        persistedSupplier.setPostalCode(supplier.getPostalCode());
        persistedSupplier.setState(supplier.getState());
        persistedSupplier.setTwitter(supplier.getTwitter());
		return repository.update(persistedSupplier);
	}
	
	@Transactional(propagation=Propagation.SUPPORTS, readOnly=true)
	public List<Supplier> findAll(String encodedId) {
		User user = userService.findByToken(encodedId);
		return repository.findByUser(user);
	}
}
