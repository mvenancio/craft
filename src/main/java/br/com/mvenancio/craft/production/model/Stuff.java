package br.com.mvenancio.craft.production.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Table(name = "stuff")
@Entity
@NamedQuery(name=Stuff.FIND_BY_USER, query="SELECT s FROM Stuff s WHERE s.user = ?1 ORDER BY s.id")
public class Stuff {

    private static final String STUFF_ID_SEQ = "stuff_id_seq";

    public static final String FIND_BY_USER = "Stuff-FindByUser";

    @Id
    @SequenceGenerator(name = STUFF_ID_SEQ, sequenceName = STUFF_ID_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = STUFF_ID_SEQ)
    private Long id;

    private String description;

    private BigDecimal unitPrice;

    private BigDecimal stock;

    @OneToOne
    @JoinColumn(name = "craft_user_id")
    private User user;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice
     *            the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the stock
     */
    public BigDecimal getStock() {
        return stock;
    }

    /**
     * @param stock
     *            the stock to set
     */
    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

}
