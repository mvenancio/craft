package br.com.mvenancio.craft.production.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.production.model.Product;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class ProductRepository extends JpaRepository<Product, Long> {

    @Override
    protected Class<Product> getEntityClass() {
        return Product.class;
    }

    public List<Product> findByUser(User user) {
        return this.query(new JpaNamedQuery<>(getEntityClass(), Product.FIND_BY_USER, user));
    }
}
