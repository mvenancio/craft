package br.com.mvenancio.craft.production.facade.json;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class ProductRequestJson extends AbstractJson {

	@NotNull
	@NotEmpty
	@Size(max = 255)
	private String description;

	@NotNull
	@DecimalMin(value = "0.01")
	@DecimalMax(value = "999999999.99")
	private BigDecimal price;

	@NotNull
	private BigDecimal percentage;
	
	private BigDecimal fixedCost;
	
	private BigDecimal depreciationCost;
	
	private BigDecimal compositionCost;
	
	private BigDecimal workCost;
	
	private BigDecimal totalCost;

	private Long productionTime;

	private Map<Long, BigDecimal> composition;

	private Map<Long, BigDecimal> costComposition;
	
	private List<Long> imageIds;
	private Long principalImageId;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the productionTime
	 */
	public Long getProductionTime() {
		return productionTime;
	}

	/**
	 * @param productionTime
	 *            the productionTime to set
	 */
	public void setProductionTime(Long productionTime) {
		this.productionTime = productionTime;
	}

	/**
	 * @return the composition
	 */
	public Map<Long, BigDecimal> getComposition() {
		return composition;
	}

	/**
	 * @param composition
	 *            the composition to set
	 */
	public void setComposition(Map<Long, BigDecimal> composition) {
		this.composition = composition;
	}

	public BigDecimal getPercentage() {
		return percentage;
	}

	public void setPercentage(BigDecimal percentage) {
		this.percentage = percentage;
	}

	public Map<Long, BigDecimal> getCostComposition() {
		return costComposition;
	}

	public void setCostComposition(Map<Long, BigDecimal> costComposition) {
		this.costComposition = costComposition;
	}

	public BigDecimal getFixedCost() {
		return fixedCost;
	}

	public void setFixedCost(BigDecimal fixedCost) {
		this.fixedCost = fixedCost;
	}

	public BigDecimal getDepreciationCost() {
		return depreciationCost;
	}

	public void setDepreciationCost(BigDecimal depreciationCost) {
		this.depreciationCost = depreciationCost;
	}

	public BigDecimal getWorkCost() {
		return workCost;
	}

	public void setWorkCost(BigDecimal workCost) {
		this.workCost = workCost;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getCompositionCost() {
		return compositionCost;
	}

	public void setCompositionCost(BigDecimal compositionCost) {
		this.compositionCost = compositionCost;
	}

	public List<Long> getImageIds() {
		return imageIds;
	}

	public void setImageIds(List<Long> imageIds) {
		this.imageIds = imageIds;
	}

	public Long getPrincipalImageId() {
		return principalImageId;
	}

	public void setPrincipalImageId(Long principalImageId) {
		this.principalImageId = principalImageId;
	}

}
