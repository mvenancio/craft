package br.com.mvenancio.craft.cost.facade.json;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.commons.joda.DatePatterns;
import br.com.mvenancio.craft.commons.joda.LocalDateDeserializer;
import br.com.mvenancio.craft.commons.joda.LocalDateSerializer;

public class DepreciationCostJson extends AbstractJson {

    private Long id;

    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String description;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "99999999.99")
    private BigDecimal value;

    @NotNull
    @Min(value = 1)
    @Max(value = Long.MAX_VALUE)
    private Integer durability;

    @JsonFormat(pattern = DatePatterns.DATE)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate startDate;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * @return the durability
     */
    public Integer getDurability() {
        return durability;
    }

    /**
     * @param durability
     *            the durability to set
     */
    public void setDurability(Integer durability) {
        this.durability = durability;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

}
