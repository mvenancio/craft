package br.com.mvenancio.craft.cost.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Table(name = "fixed_cost")
@Entity
@NamedQueries({
    @NamedQuery(name = FixedCost.FIND_BY_USER, query = "SELECT f FROM FixedCost f WHERE f.user = ?1 ORDER BY f.id"),
    @NamedQuery(name = FixedCost.GET_TOTAL_COST, query = "SELECT SUM(f.value) FROM FixedCost f WHERE f.user = ?1")
})
public class FixedCost {

    private static final String FIXED_COSTS_ID_SEQ = "fixed_cost_id_seq";

    public static final String FIND_BY_USER = "FixedCost-findByUser";
    public static final String GET_TOTAL_COST = "FixedCost-getTotalCost";

    @Id
    @SequenceGenerator(name = FIXED_COSTS_ID_SEQ, sequenceName = FIXED_COSTS_ID_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = FIXED_COSTS_ID_SEQ)
    private Long id;

    private String description;

    private BigDecimal value;

    @OneToOne
    @JoinColumn(name = "craft_user_id")
    private User user;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

}
