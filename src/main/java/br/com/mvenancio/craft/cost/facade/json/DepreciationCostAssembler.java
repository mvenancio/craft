package br.com.mvenancio.craft.cost.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.cost.model.DepreciationCost;

@Component
public class DepreciationCostAssembler implements Assembler<DepreciationCost, DepreciationCostJson> {

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public DepreciationCostJson toJson(DepreciationCost entity) {
        final DepreciationCostJson json = new DepreciationCostJson();
        json.setDescription(entity.getDescription());
        json.setId(entity.getId());
        json.setValue(entity.getValue());
        json.setDurability(entity.getDurability());
        json.setStartDate(entity.getStartDate());
        return json;
    }

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<DepreciationCostJson> toJson(List<DepreciationCost> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }

}
