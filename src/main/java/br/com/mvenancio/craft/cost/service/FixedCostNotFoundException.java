package br.com.mvenancio.craft.cost.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class FixedCostNotFoundException extends CraftException {

    /**
     *
     */
    private static final long serialVersionUID = 9198070491530805512L;

    public FixedCostNotFoundException(Long id) {
        super("Fixed cost not found: id " + id, HttpStatus.NOT_FOUND);
    }

}
