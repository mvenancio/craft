package br.com.mvenancio.craft.cost.facade.json;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.mvenancio.craft.commons.facade.AbstractJson;

public class FixedCostJson extends AbstractJson {

    private Long id;

    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String description;

    @NotNull
    @DecimalMin(value = "0.01")
    @DecimalMax(value = "99999999.99")
    private BigDecimal value;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

}
