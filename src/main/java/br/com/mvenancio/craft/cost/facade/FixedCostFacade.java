package br.com.mvenancio.craft.cost.facade;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvenancio.craft.commons.facade.AbstractJson;
import br.com.mvenancio.craft.cost.facade.json.FixedCostAssembler;
import br.com.mvenancio.craft.cost.facade.json.FixedCostJson;
import br.com.mvenancio.craft.cost.service.FixedCostService;
import br.com.mvenancio.craft.security.facade.HeaderSecurity;
import br.com.mvenancio.craft.util.ResponseUtil;

@RestController
@RequestMapping("/fixed-costs")
public class FixedCostFacade {

	@Autowired
	private ResponseUtil<FixedCostJson> responseUtil;

	@Autowired
	private FixedCostAssembler assembler;

	@Autowired
	private FixedCostService service;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> findAll(
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.findAll(encodedUserId)));
	}

	@RequestMapping(value = "/total", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTotalCost(@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(service.getTotalCost(encodedUserId), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> post(@Validated @RequestBody FixedCostJson request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(
				assembler.toJson(service.add(request.getDescription(), request.getValue(), encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> put(@PathVariable("id") @NotNull Long id,
			@Validated @RequestBody FixedCostJson request,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(
				assembler.toJson(service.update(id, request.getDescription(), request.getValue(), encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> get(@PathVariable("id") @NotNull Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		return responseUtil.buildResponse(assembler.toJson(service.get(id, encodedUserId)));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AbstractJson> delete(@PathVariable("id") @NotNull Long id,
			@RequestHeader(value = HeaderSecurity.USER_ID_PARAM) String encodedUserId) {
		service.delete(id, encodedUserId);
		return responseUtil.buildResponse(null, HttpStatus.OK);
	}
}
