package br.com.mvenancio.craft.cost.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.cost.model.DepreciationCost;
import br.com.mvenancio.craft.cost.repository.DepreciationCostRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class DepreciationCostService {

    @Autowired
    private DepreciationCostRepository repository;

    @Autowired
    private UserService userService;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<DepreciationCost> findAll(String encodedId) {
        User user = userService.findByToken(encodedId);
        return repository.findByUser(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public DepreciationCost add(String description, BigDecimal value, Integer durability, LocalDate startDate,
            String encodedId) {
        User user = userService.findByToken(encodedId);

        final DepreciationCost cost = new DepreciationCost();
        cost.setDescription(description);
        cost.setDurability(durability);
        cost.setValue(value);
        cost.setStartDate(startDate);
        cost.setUser(user);
        return repository.add(cost);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public DepreciationCost update(Long id, String description, BigDecimal value, Integer durability,
            LocalDate startDate, String encodedId) {
        final DepreciationCost cost = get(id, encodedId);
        cost.setDescription(description);
        cost.setDurability(durability);
        cost.setValue(value);
        cost.setStartDate(startDate);
        return repository.add(cost);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public DepreciationCost get(Long id, String encodedId) {

        User user = userService.findByToken(encodedId);

        final DepreciationCost depreciationCost = repository.get(id);
        if (depreciationCost == null || !depreciationCost.getUser().equals(user)) {
            throw new DepreciationCostNotFoundException(id);
        }
        return depreciationCost;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id, String encodedUserId) {
        final DepreciationCost cost = get(id, encodedUserId);
        repository.remove(cost);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BigDecimal getTotalCost(String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        BigDecimal totalCost = repository.getTotalCost(user);
        return totalCost == null ? BigDecimal.ZERO : totalCost;
    }

}
