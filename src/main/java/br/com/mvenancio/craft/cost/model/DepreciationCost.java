package br.com.mvenancio.craft.cost.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.mvenancio.craft.security.model.User;

@Table(name = "depreciation_cost")
@Entity
@NamedQueries({
    @NamedQuery(name = DepreciationCost.FIND_BY_USER,
            query = "SELECT d FROM DepreciationCost d WHERE d.user = ?1 ORDER BY d.id"),
    @NamedQuery(name = DepreciationCost.GET_TOTAL_COST,
    query = "SELECT SUM(d.value/d.durability) FROM DepreciationCost d WHERE d.user = ?1")
})
public class DepreciationCost {

    private static final String DEPRECIATION_COST_ID_SEQ = "depreciation_cost_id_seq";
    public static final String FIND_BY_USER = "DepreciationCost-findByUser";
    public static final String GET_TOTAL_COST = "DepreciationCost-getTotalCost";

    @Id
    @SequenceGenerator(name = DEPRECIATION_COST_ID_SEQ, sequenceName = DEPRECIATION_COST_ID_SEQ, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = DEPRECIATION_COST_ID_SEQ)
    private Long id;

    private String description;

    private BigDecimal value;

    private Integer durability;

    @Column(name = "start_date")
    private LocalDate startDate;

    @OneToOne
    @JoinColumn(name = "craft_user_id")
    private User user;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * @return the durability
     */
    public Integer getDurability() {
        return durability;
    }

    /**
     * @param durability
     *            the durability to set
     */
    public void setDurability(Integer durability) {
        this.durability = durability;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

}
