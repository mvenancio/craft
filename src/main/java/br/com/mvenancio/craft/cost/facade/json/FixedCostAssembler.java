package br.com.mvenancio.craft.cost.facade.json;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.mvenancio.craft.commons.facade.Assembler;
import br.com.mvenancio.craft.cost.model.FixedCost;

@Component
public class FixedCostAssembler implements Assembler<FixedCost, FixedCostJson> {

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.lang.Object)
     */
    @Override
    public FixedCostJson toJson(FixedCost entity) {
        final FixedCostJson json = new FixedCostJson();
        json.setDescription(entity.getDescription());
        json.setId(entity.getId());
        json.setValue(entity.getValue());
        return json;
    }

    /*
     * (non-Javadoc)
     *
     * @see br.com.mvenancio.craft.commons.facade.Assembler#toJson(java.util.List)
     */
    @Override
    public List<FixedCostJson> toJson(List<FixedCost> entities) {
        return entities == null ? null : entities.stream().map(entity -> toJson(entity)).collect(Collectors.toList());
    }

}
