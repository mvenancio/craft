package br.com.mvenancio.craft.cost.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.mvenancio.craft.cost.model.FixedCost;
import br.com.mvenancio.craft.cost.repository.FixedCostRepository;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.service.UserService;

@Service
public class FixedCostService {

    @Autowired
    private FixedCostRepository repository;

    @Autowired
    private UserService userService;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<FixedCost> findAll(String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        return repository.findByUser(user);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public FixedCost add(String description, BigDecimal value, String encodedUserId) {

        User user = userService.findByToken(encodedUserId);

        final FixedCost cost = new FixedCost();
        cost.setDescription(description);
        cost.setValue(value);
        cost.setUser(user);
        return repository.add(cost);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public FixedCost update(Long id, String description, BigDecimal value, String encodedUserId) {
        final FixedCost cost = get(id, encodedUserId);
        cost.setDescription(description);
        cost.setValue(value);
        return repository.add(cost);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public FixedCost get(Long id, String encodedUserId) {

        User user = userService.findByToken(encodedUserId);

        final FixedCost cost = repository.get(id);
        if (cost == null || !cost.getUser().equals(user)) {
            throw new FixedCostNotFoundException(id);
        }
        return cost;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(Long id, String encodedUserId) {
        final FixedCost cost = get(id, encodedUserId);
        repository.remove(cost);
    }

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BigDecimal getTotalCost(String encodedUserId) {
        User user = userService.findByToken(encodedUserId);
        BigDecimal totalCost = repository.getTotalCost(user);
        return totalCost == null ? BigDecimal.ZERO : totalCost;
    }

}
