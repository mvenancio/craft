package br.com.mvenancio.craft.cost.repository;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.mvenancio.craft.commons.repository.JpaNamedQuery;
import br.com.mvenancio.craft.commons.repository.JpaRepository;
import br.com.mvenancio.craft.cost.model.FixedCost;
import br.com.mvenancio.craft.security.model.User;

@Repository
public class FixedCostRepository extends JpaRepository<FixedCost, Long> {

	@Override
	protected Class<FixedCost> getEntityClass() {
		return FixedCost.class;
	}

	public List<FixedCost> findByUser(User user) {
		return this.query(new JpaNamedQuery<>(getEntityClass(), FixedCost.FIND_BY_USER, user));
	}

	public BigDecimal getTotalCost(User user) {
		TypedQuery<BigDecimal> query = em.createNamedQuery(FixedCost.GET_TOTAL_COST, BigDecimal.class);
		query.setParameter(1, user);
		return query.getSingleResult();
	}

}
