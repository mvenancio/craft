package br.com.mvenancio.craft.cost.service;

import org.springframework.http.HttpStatus;

import br.com.mvenancio.craft.commons.service.CraftException;

public class DepreciationCostNotFoundException extends CraftException {

    private static final long serialVersionUID = 3925028974751893923L;

    public DepreciationCostNotFoundException(Long id) {
        super("Depreciation cost not found: id " + id, HttpStatus.NOT_FOUND);
    }

}
