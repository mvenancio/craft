package br.com.mvenancio.craft.security.service;

import org.junit.Test;
import org.springframework.util.Base64Utils;

public class UserServiceTest {

    @Test
    public void testEncrypPassword() {
        System.out.println(new UserService().encrypPassword("123456"));
    }

    @Test
    public void base64() {
        System.out.println(Base64Utils.encodeToString("1".getBytes()));
    }

}
