package br.com.mvenancio.craft.util;

import javax.mail.MessagingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.mvenancio.craft.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class, initializers = ConfigFileApplicationContextInitializer.class)
@Profile("test")
public class MailUtilIT {

	@Autowired
	private MailUtil mailUtil;

	@Test
	public void testSendEmail() throws MessagingException {
		mailUtil.sendEmail("mateusvenan@gmail.com", "");
	}

}
