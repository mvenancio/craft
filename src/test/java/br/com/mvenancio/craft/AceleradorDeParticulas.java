package br.com.mvenancio.craft;

import java.util.Scanner;

public class AceleradorDeParticulas {

	public static void main(String args[]) throws Exception {
		Scanner in = new Scanner(System.in);
		int atoms = in.nextInt();
		atoms = atoms <= 1_000_000_000 ? atoms : 1_000_000_000;
		int size = in.nextInt();
		size = size <= 100 ? size : 100;
		int regionsSize = in.nextInt();
		regionsSize = regionsSize <= 100 ? regionsSize : 100;
		int[] regions = new int[regionsSize];

		for (; atoms > 0; atoms--) {
			regions[0]++;
			regions = adjustRegions(regions, size);
		}
		for (int ix : regions) {
			System.out.print(ix+" ");
		}

	}

	private static int[] adjustRegions(int[] regions, int size) {
		int ix = 0;
		while (ix <= regions.length-1 && regions[ix] > size) {
			regions[ix] = 0;
			ix++;
			if (ix <= regions.length-1) {
				regions[ix]++;
			}
		}
		return regions;
	}
}
