package br.com.mvenancio.craft.signature.config;

import org.junit.Assert;
import org.junit.Test;

import br.com.mvenancio.craft.signature.dto.BillingErrorWrapper;

public class PagSeguroErrorDecoderTest {

	@Test
	public void test() {
		String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?><errors><error><code>17022</code><message>invalid pre-approval status to execute the requested operation. Pre-approval status is PENDING.</message></error></errors>";
		BillingErrorWrapper parse = new PagSeguroErrorDecoder().parse(xml);
		Assert.assertNotNull(parse);
	}

}
