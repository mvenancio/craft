package br.com.mvenancio.craft.signature.repository;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.mvenancio.craft.commons.TransactionalTestCase;
import br.com.mvenancio.craft.security.model.Status;
import br.com.mvenancio.craft.security.model.User;
import br.com.mvenancio.craft.security.repository.UserRepository;
import br.com.mvenancio.craft.signature.model.Signature;
import br.com.mvenancio.craft.signature.model.SignatureStatus;
import br.com.mvenancio.craft.signature.repository.SignatureRepository;

public class SignatureRepositoryIT extends TransactionalTestCase {

	@Autowired
	private SignatureRepository repository;
	@Autowired
	private UserRepository userRepository;

	User user;

	@Before
	public void setUp() {
		User entity = new User();
		entity.setEmail("email@email.com");
		entity.setPassword("123123123");
		entity.setToken("123123123");
		entity.setCreationTime(LocalDateTime.now());
		entity.setStatus(Status.AUTHORIZED);
		user = userRepository.add(entity);
	}

	@Test
	public void test() {
		Signature signature = new Signature();
		signature.setUser(user);
		signature.setCreationDate(LocalDateTime.now());
		signature.setStatus(SignatureStatus.PENDING);
		repository.save(signature);
		
		Signature signatureFound = repository.findByUser(user);
		Assert.assertNotNull(signatureFound);
		Assert.assertEquals(user.getId(), signatureFound.getUser().getId());
	}

}
