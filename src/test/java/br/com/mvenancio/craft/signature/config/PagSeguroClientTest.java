package br.com.mvenancio.craft.signature.config;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import br.com.mvenancio.craft.commons.TransactionalTestCase;
import br.com.mvenancio.craft.signature.config.PagSeguroClient;
import br.com.mvenancio.craft.signature.dto.CheckoutRequest;
import br.com.mvenancio.craft.signature.dto.Item;
import br.com.mvenancio.craft.signature.dto.PreApproval;
import br.com.mvenancio.craft.signature.dto.PreApprovalCancelResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalQueryResponse;
import br.com.mvenancio.craft.signature.dto.PreApprovalRequest;
import br.com.mvenancio.craft.signature.dto.Sender;
import br.com.mvenancio.craft.signature.dto.TransactionDetailResponse;
import br.com.mvenancio.craft.signature.dto.TransactionSearchResponse;

@Ignore
public class PagSeguroClientTest extends TransactionalTestCase {

	@Value("${pagseguro.email}")
	private String email;

	@Value("${pagseguro.token}")
	private String token;

	@Value("${security.user.name}")
	private String name;

	@Autowired
	private PagSeguroClient client;

	@Test
	public void testCreateSignature() {
		String finalDate = LocalDateTime.now().plusYears(2).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

		PreApprovalRequest request = new PreApprovalRequest();
		request.setRedirectURL("http://srartesao.com.br/system");
		PreApproval preApproval = new PreApproval();
		preApproval.setCharge("auto");
		preApproval.setName("Assinatura Sr. Artesão");
		preApproval.setDetails("Assinatura mensal da plataforma de precificação e marketplace Sr. Artesão");
		preApproval.setAmountPerPayment(BigDecimal.TEN.setScale(2));
		preApproval.setPeriod("MONTHLY");
		preApproval.setFinalDate(finalDate);
		preApproval.setMaxTotalAmount(BigDecimal.TEN.multiply(BigDecimal.TEN).setScale(2));

		request.setPreApproval(preApproval);
		Sender sender = new Sender();
		sender.setName("Mateus Antonio Venancio");
		sender.setEmail("c87667523784172904523@sandbox.pagseguro.com.br");
		request.setSender(sender);
		System.out.println(client.createSignature(email, token, request));
	}

	@Test
	public void testDetailSignature() {
		String transaction = "C5DCB223DFDF129BB4C96F984AFF69CD";

		PreApprovalQueryResponse signatureDetail = client.signatureDetail(email, token, transaction);
		System.out.println(signatureDetail);
	}

	@Test
	public void testCancelSignature() {
		String transaction = "C5DCB223DFDF129BB4C96F984AFF69CD";

		PreApprovalCancelResponse cancel = client.cancel(email, token, transaction);
		System.out.println(cancel);
	}

	@Test
	public void testCreatePayent() {
		CheckoutRequest request = new CheckoutRequest();
		Item item = new Item();
		request.setItems(Arrays.asList(item));
		request.setReference("123321");

		System.out.println(client.createPayent(email, token, request));
	}

	@Test
	public void testSearch() {
		String initialDate = LocalDateTime.now().withDayOfMonth(1).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		String finalDate = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);

		TransactionSearchResponse search = client.search(email, token, initialDate, finalDate);
		System.out.println(search);
	}

	@Test
	public void testDetail() {
		String transaction = "3FA80AF4-4B3C-4E45-A9E8-ED2B5BE7B6F7";

		TransactionDetailResponse search = client.detail(email, token, transaction);
		System.out.println(search);
	}

}
