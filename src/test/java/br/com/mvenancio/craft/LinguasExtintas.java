package br.com.mvenancio.craft;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;

public class LinguasExtintas {

	public static void main(String[] args) {
		Scanner sysIn = new Scanner(System.in);
		int testCases = sysIn.nextInt();
		testCases = testCases <= 20 ? testCases : 20;
		for (int ix = 0; ix < testCases; ix++) {
			int extintedWords = Integer.valueOf(sysIn.next());
			extintedWords = extintedWords <= 100 ? extintedWords : 100;
			int newLanguagePhrases = Integer.valueOf(sysIn.next());

			newLanguagePhrases = newLanguagePhrases <= 50 ? newLanguagePhrases : 50;
			List<String> oldLanguageWords = getExtintedWords(sysIn, extintedWords);

			List<String> readedWords = getWordsFromLines(sysIn, newLanguagePhrases);
			List<String> answers = new ArrayList<>();
			for (String oldWord : oldLanguageWords) {
				answers.add(readedWords.contains(oldWord) ? "YES" : "NO");
			}
			System.out.println(String.join(" ", answers));
		}

	}

	private static List<String> getExtintedWords(Scanner sysIn, int extintedWords) {
		List<String> oldLanguageWords = new ArrayList<>();
		while (oldLanguageWords.size() != extintedWords) {
			oldLanguageWords.add(sysIn.next());
		}
		return oldLanguageWords;
	}

	private static List<String> getWordsFromLines(Scanner sysIn, int newLanguagePhrases) {
		List<String> readedWords = new ArrayList<>();
		for (int line = 0; line < newLanguagePhrases; line++) {
			int words = Integer.valueOf(sysIn.next());
			words = words <= 50 ? words : 50;
			List<String> phraseWords = getExtintedWords(sysIn, words);
			readedWords.addAll(phraseWords);
		}
		return readedWords;
	}
}
